// ---------------------------------------------------------------------------
#ifndef BPTokenizerH
#define BPTokenizerH

#include <vector>
#include <fstream>
#include <cstdlib>
#include <map>
// ---------------------------------------------------------------------------

struct lineRecord {
	char *lineStart, *lineEnd;
	int nTokens, nBytes;

	std::vector<char*>fields;
};

class BPTokenizer {
private:
    std::vector<char>fileData;
    std::vector<char>fileDataCopy;
    std::vector<lineRecord>lines;
    std::vector<std::tuple<size_t,size_t,std::string>> m_fileInsertionList;
    size_t m_searchLine,m_searchField;
    char m_commentString[64];//buff for null terminated comment string used for efficiency
    std::vector<char>m_delimiters;
    bool m_good;
    bool isDelimiter(char c);
    size_t getFieldAddress(size_t lineNumber, size_t fieldNumber);
    size_t getNextFieldAddress(size_t lineNumber, size_t fieldNumber);

   //stuff for managing eading of tables by column
    size_t m_dataTableStart, m_dataTableEnd,m_dataTableNCols,m_titleLine;
    std::map<std::string,int> m_titleIndex;
    std::vector<size_t> m_hitList;

public:
    BPTokenizer(std::ifstream &fileStream);
    BPTokenizer(std::filesystem::path p) ;
    BPTokenizer();
    void initialize(std::ifstream &fileStream);
    bool isGood(){return(m_good);}
    //return true if the file loaded and initialized ok

    void setDelimiters(std::string delimiterList);
    //string containng one or more token delimeters

    void setCommentString(const std::string &commentString);
    //string which if matched indicates a line is a comment line

	void Tokenize();
    //Do work to tokenize the entire file in place.. do this after setting delimeter list

    //Access methods
    bool getDouble(const size_t lineNumber, const size_t fieldNumber, double &value)const;
    bool getString(const size_t lineNumber, const size_t fieldNumber,std::string &string)const;
    bool getString(const size_t lineNumber,std::string &string)const;
    char getChar(const size_t lineNumber,size_t fieldNumber)const;
    bool getInt(const size_t lineNumber, const size_t fieldNumber,int &value)const;
    bool split(size_t linenumber, std::vector<std::string> &v);
    //Returns std::vector<std::string> for line number

    //Searching methods return row and column of found field
    bool find( const std::string &StringtoFind, size_t &lineNumber,  size_t &fieldNumber);
    bool findNext( const std::string &StringtoFind, size_t &lineNumber,  size_t &fieldNumber);
    void  resetFind(){ m_searchLine=0; m_searchField=0;}

    char* GetField(const size_t lineNumber, const size_t fieldNumber)const;
    bool isCommentLine(size_t lineNumber);

    size_t nlines() {return ((int) lines.size());}
    //numbe rof lines in the file

    size_t nfields(size_t linenumber);
    //returns number of columns for line number

    //File modification methods
    bool addAfter(const size_t lineNumber, const size_t fieldNumber,std::string &stringToAdd);
    //Queues the string stringToAdd to be inserted into the file when written back to disk using saveFile
    bool isFileAugmented(){return (m_fileInsertionList.size()!=0);}
    bool saveFile(std::ofstream &outFile);

    bool isLegit(size_t lineNumber,  size_t fieldNumber);

    //Data Tables
    //Your file may contain many data tables which are sequences of lines with the same numbe rof columns and where
    // the data in each column are declared in a column title line which may occur anywhere in the file
    //To pull data out of a table call initDataTable(size_t titleLine, size_t lineStart, size_t lineEnd)
    //giving it the 0-based line number to find the list of column titles and the range of data lines
    // once a table has been initialized

    bool initDataTable(size_t titleLine, size_t lineStart, size_t lineEnd);
    bool getColumnAsDouble(std::string colName, std::vector<double> &values)const;
    bool getColumnAsDouble(std::vector<std::string> colNames, std::vector<double> &values)const;

    bool getColumnAsString(std::string colName, std::vector<std::string> &values)const;
    bool getColumnAsInt(std::string colName, std::vector<int> &values)const;
    bool getColumnAsChar(std::string colName, std::vector<char> &values)const;
    bool columnTitleExists(std::string colName);

};

#endif
