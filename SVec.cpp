#include "SVec.h"
#include <cmath>
#include <array>

SVec::SVec() : x(0), y(0), z(0)
{
}

SVec::SVec(const double& x_i, const double& y_i, const double& z_i) : x(x_i), y(y_i), z(z_i)
{
}

SVec::SVec(const std::array<float,3>& array) : SVec(array[0], array[1], array[2])
{
}

SVec::SVec(const std::array<double,3>& array) : SVec(array[0], array[1], array[2])
{

}

//SVec::SVec(double&& x_i, double&& y_i, double&& z_i) : x(std::move(x_i)), y(std::move(y_i)), z(std::move(z_i))
//{
//}

SVec::SVec(const double& dip, const double& dipdir)
{
    using namespace std;
    static const double deg2rad = 0.01754532;
    double aaz  = ( 90.0 - dipdir ) * deg2rad;
    double ddip = dip * deg2rad;
    x =  cos(ddip) * cos(aaz);
    y =  cos(ddip) * sin(aaz);
    z = -sin(ddip);
}

SVec::SVec(const SVec& other) : x(other.x), y(other.y), z(other.z)
{
}

SVec::SVec(SVec&& other) : x(std::move(other.x)), y(std::move(other.y)), z(std::move(other.z))
{
}

void SVec::Normalize()
{
    double l = Length();

    if(l!=0.0)
    {
       x /= l;
       y /= l;
       z /= l;
    }
}

double SVec::NormalizeL()
{
    double l = sqrt(x*x+y*y+z*z);
    if(l!=0.0)
    {
       x /= l;
       y /= l;
       z /= l;
    }
    return(l);
}

SVec SVec::Normalized() const
{
    double l = Length();

    if(l!=0.0)
    {
       return SVec(x/l, y/l, z/l);
    }
     return SVec(0, 0, 0);
}


 SVec& SVec::operator =(const SVec& second)
{
    x = second.x;
    y = second.y;
    z = second.z;
    return *this ;
}

 SVec& SVec::operator =(SVec&& second)
{
    x = second.x;
    y = second.y;
    z = second.z;
    return *this;
}

bool SVec::operator ==( const SVec& second ) const
{
    const double tolerance = 0.000001;
    return std::fabs(x - second.x) < tolerance &&
        std::fabs(y - second.y) < tolerance &&
        std::fabs(z - second.z) < tolerance ;
}

bool SVec::equals(const SVec& other, double tolerance) const
{
    return std::abs(x-other.x) < tolerance && std::abs(y-other.y) < tolerance && std::abs(z-other.z) < tolerance;
}

bool SVec::operator !=( const SVec& second ) const
{
    return !(*this == second);
}

SVec SVec::operator +( const SVec& second ) const
{
    return SVec(x+second.x, y+second.y, z+second.z);
}
/*
SVec& SVec::operator+=(const SVec& second)
{
    *this = *this + second;
    return *this;
}*/

SVec SVec::operator-(const SVec& second) const
{
    return SVec(x-second.x, y-second.y, z-second.z);
}

SVec& SVec::operator-=(const SVec& second)
{
    *this = *this - second;
    return *this;
}

double SVec::operator *(const SVec& second ) const  // dot product
{
    return x * second.x + y * second.y + z * second.z;
}

SVec SVec::operator /(const SVec& second ) const  // cross product
{
    return SVec( y * second.z - z * second.y,
                z * second.x - x * second.z,
                x * second.y - y * second.x );
}

SVec SVec::operator /(const double& c) const
{
    if( c == 0.0 )
       return SVec(0, 0, 0);
    return SVec(x/c, y/c, z/c);
}


/*
SVec& SVec::operator *=(double k)
{
    *this = *this * k;
    return *this;
}
*/
SVec& SVec::operator /=(double k)
{
    *this = *this/k;
    return *this;
}

std::ostream &operator<< (std::ostream &os, const SVec &v)
{
    os << v.x << ", " << v.y << ", " << v.z;
    return os;
}
/*
double SVec::Length() const
{
    return std::sqrt(x*x+y*y+z*z);
}

*/
//std::array<double,3>* SVec::inPlaceArray() const
//{
//    return (std::array<double,3>*)(&x);
//}

SVec& SVec::operator =(const std::array<float,3>& second)
{
    x = (double)second[0];
    y = (double)second[1];
    z = (double)second[2];
    return *this;
}

SVec& SVec::operator =(const std::array<double,3>& second)
{
    x = second[0];
    y = second[1];
    z = second[2];
    return *this;
}

double SVec::angle(const SVec& other) const
{
    return std::acos(*this*other);
}

SVec4::SVec4(double r_i, double g_i, double b_i, double a_i) : SVec4()
{
    r = r_i;
    g = g_i;
    b = b_i;
    a = a_i;
}

SVec4& SVec4::operator=(const SVec4& other)
{
    r = other.r;
    g = other.g;
    b = other.b;
    a = other.a;
    return *this;
}

std::array<float,4> SVec4::toFloat4() const
{
    return {(float) r, (float) g, (float) b, (float) a};
}

SVec4 SVec4::fromArray(const std::array<float,4>& array)
{
    SVec4 color;
    color.r = array[0];
    color.g = array[1];
    color.b = array[2];
    color.a = array[3];
    return color;
}

std::vector<unsigned char> SVec4::toUnsignedCharVec() const
{
    return { (unsigned char) (r*255.), (unsigned char) (g*255.), (unsigned char) (b*255.), (unsigned char) (a*255.)};
}

SVec SVec::fromArray(const std::array<float,3>& array)
{
    return SVec{array[0],array[1],array[2]};
}

SVec SVec::fromArray(const std::array<double,3>& array)
{
    return SVec{array[0],array[1],array[2]};
}

std::array<float,3> SVec::toFloat3() const
{
    return {(float)x,(float)y,(float)z};
}

std::array<double,3> SVec::toDouble3() const
{
    return {x,y,z};
}

SVec SVec::Rotate(const SVec &rotationAxis, double radAngle){
     SVec k=rotationAxis;
     k.Normalize();
     //Rodriguez Formula
     double ct=cos(radAngle);
     double st=sin(radAngle);
     SVec rotated=(*this)*ct+(k/(*this))*st+k*(k*(*this))*(1-ct);
     return(rotated);
}
void SVec::RotateSelf(const SVec &rotationAxis, double radAngle){
     SVec k=rotationAxis;
     k.Normalize();
     //Rodriguez Formula
     double ct=cos(radAngle);
     double st=sin(radAngle);
     SVec rotated=(*this)*ct+(k/(*this))*st+k*(k*(*this))*(1-ct);
     x=rotated.x;
     y=rotated.y;
     z=rotated.z;
}

