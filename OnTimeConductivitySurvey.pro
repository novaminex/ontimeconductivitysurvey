QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    BPTokenizer.cpp \
    EField.cpp \
    SVec.cpp \
    SVec.cpp \
    SVec.cpp \
    SVec.cpp \
    investigateRawData.cpp \
    main.cpp \
    mainwindow.cpp \
    novaleastsquares.cpp \
    ontimesignalsurvey.cpp \
    pugixml.cpp \
    sm24_stk_reader.cpp \
    vectorcompstackedwaveform.cpp

HEADERS += \
    BPTokenizer.h \
    EField.h \
    SVec.h \
    SVec.h \
    investigateRawData.h \
    mainwindow.h \
    novaleastsquares.h \
    ontimesignalsurvey.h \
    pugiconfig.hpp \
    pugixml.hpp \
    rstation.h \
    sm24_stk_reader.h \
    vectorcompstackedwaveform.h

FORMS += \
    mainwindow.ui

INCLUDEPATH += "C:/local/eigen-3.3.9/"
INCLUDEPATH += "C:/local/boost_1_75_0/"

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
