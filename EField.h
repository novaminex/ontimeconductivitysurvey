//---------------------------------------------------------------------------
#ifndef EFieldH
#define EFieldH

#include <vector>
#include <cmath>
#include <SVec.h>
//---------------------------------------------------------------------------

//This class  computes the vector potential of a polyline (collection of lines) of current carrying a unit ampere
//To get the EField, take the time derivative ie multiply by j*omega in frequency domain
// units are SI

class VectorPotentialofPolyLine{
	private:
      std::vector<SVec>m_PolyLine;
      double m_delta;
      SVec VectorPotentialofSegment(const SVec &start, const SVec &end, const SVec &location) const;
	public:
      VectorPotentialofPolyLine(const std::vector<SVec> PolyLine);
      SVec getVectorPotential(const SVec &location) const;
      SVec curlE(const SVec &location);
	};

//---------------------------------------------------------------------------------
	class HFieldOfLoop{
        ///Give the H-field value from the loop (polyline) at a given location
	private:
      std::vector<SVec>m_PolyLine;
	  bool m_closedLoop;
	public:
      HFieldOfLoop(const std::vector<SVec> PolyLine,bool closedloop);
      SVec getHField( const SVec &location);
	};
//---------------------------------------------------------------------------------

#endif
