#include "novaleastsquares.h"
using namespace std;

template<typename T>
bool NovaLeastSquares<T>::computeUsingNormalEq()
{
    ///Do calculation
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> X,Y,W;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> B; //soln
    for(int i = 0; i<Xd.cols(); i++){
        for(int j = 0; j<Xd.rows(); j++){
            X(i,j)=Xd(i,j)*sqrt(Wd(j,1));
            Y(i,j)=Yd(i,j)*sqrt(Wd(j,1));
        }
        B=((X.transpose())*X).ldlt().solve(X.transpose() * Y); //Using normal equations.
        Bd=B;
        return true;
    }
    doneCalculating=true;
    return false;
}

template<typename T>
std::optional<std::vector<T> > NovaLeastSquares<T>::residualsAsPercentage()
{
    /// (measured - fit)/fit X 100%
    if(!doneCalculating) return nullopt;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> r; //residuals
    r = (Yd-Xd*Bd).cwiseQuotient(Xd*Bd);
    vector<T> residuals;
    residuals.reserve(r.rows());
    for(int i=0;i<r.rows();i++){
        residuals.push_back(r(i,0));
    }
    return residuals;
}

template<typename T>
std::optional<std::vector<T> > NovaLeastSquares<T>::residuals()
{
    /// (measured - fit)
    if(!doneCalculating) return nullopt;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> r; //residuals
    r = Yd-Xd*Bd;
    vector<T> residuals;
    residuals.reserve(r.rows());
    for(int i=0;i<r.rows();i++){
        residuals.push_back(r(i,0));
    }
    return residuals;
}

template<typename T>
std::optional<std::vector<T> > NovaLeastSquares<T>::fit()
{
    /// Apply Xd*Bd
    if(!doneCalculating) return nullopt;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> f; //
    f =Xd*Bd;
    vector<T> fit;
    fit.reserve(f.rows());
    for(int i=0;i<f.rows();i++){
        fit.push_back(f(i,0));
    }
    return fit;
}

template<typename T>
bool errorChecks(vector<vector<T> > &X, vector<T> &Y){
    //Equal lengths
    const size_t L=Y.size();
    for(vector<T> &x:X){
        if(x.size() != L){
            return true;
        }
    }
    return false;
}

template<typename T>
bool errorChecks(vector<vector<T> > &X, vector<T> &Y, vector<T> &W){
    //Equal lengths
    const size_t L=Y.size();
    for(vector<T> &x:X){
        if(x.size() != L){
            return true;
        }
    }
    if(L!=W.size()){
        return true;
    }
    return false;
}

template<typename T>
bool NovaLeastSquares<T>::load(vector<vector<T> > &X, vector<T> &Y)
{
    ///Class will store its own copy of data so user can perform more than one computation if needed.
    /// (i.e. get null vectors through SVD, percentages error on residuals etc...)
    if(errorChecks(X,Y)){
        return true;
    }
    vector<T> W;
    W.assign(Y.size(),1.0);
    switchToEigen(X,Y,W);
    return true;
}

template<typename T>
bool NovaLeastSquares<T>::load(std::vector<std::vector<T> > &X, std::vector<T> &Y, std::vector<T> &W)
{
    ///This loading scheme allows for user selected weighting.
    if(errorChecks(X,Y,W)){
        return true;
    }
    switchToEigen(X,Y,W);
    return true;
}

template<typename T>
void NovaLeastSquares<T>::switchToEigen(std::vector<std::vector<T> > &X, std::vector<T> &Y, std::vector<T> &W)
{
    ///Load data into class variables.
    //doubles
    Xd = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>(X.front().size(),X.size());
    Yd = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>(X.front().size(),1);
    Wd = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>(X.front().size(),1);
    for(size_t i=0;i<X.size();i++){
        for(size_t j=0;j<X.size();j++){
            Xd(i,j)=X[i][j];
        }
    }
    for(size_t j=0;j<X.front().size();j++){
        Yd(j,1)=Y[j];
        Wd(j,1)=abs(W[j]);
    }
}

// Explicit template instantiation
template class NovaLeastSquares<double>;
template class NovaLeastSquares<float>;
//Can expand to use complex numbers. TO DO
