#include "ontimesignalsurvey.h"
#include "vectorcompstackedwaveform.h"
#include "EField.h"
#include "novaleastsquares.h"

#include <exception>
#include <stdexcept>
#include <string>
#include <numeric>
#include <functional>
#include <filesystem>
#include <optional>
#include <fstream>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SparseCore>

using namespace std;
using namespace OnTimeSignalSurveyTools;

template <typename X,typename Y>
bool checkLocationsAreIdentical(X &a,Y &b){
    ///Special error check
    if(a.size() != b.size()){
        throw runtime_error("mismatch in array sizes in specialProcessing: "+to_string(a.size())+":"+to_string(b.size()));
    }
    bool allEqual=true;
    for(size_t i=0;i<a.size();i++){
        bool utmE_is_different = a[i].first.x!=b[i].first.x;
        bool utmN_is_different = a[i].first.y!=b[i].first.y;
        if(utmE_is_different || utmN_is_different){
            allEqual=false;
            //            throw runtime_error("Problem with entry: "+to_string(i));
        }
    }
    return allEqual;
}

bool sameLocation (pair<SVec, double> &a, pair<SVec, double> &b) {
    return(a.first==b.first);
}

template <typename T>
bool from_min_UTME_and_min_UTMN(T &a,T &b){
    bool utmE_is_ahead = a.first.x>b.first.x;
    bool utmN_is_ahead = a.first.y>b.first.y;
    return utmN_is_ahead&&utmE_is_ahead;
}

class SeriesType{
public:
    static SeriesType Raw(){
        SeriesType type(true);
        return type;
    }
    static SeriesType Clean(){
        SeriesType type(false);
        return type;
    }
    const bool useRawData;
private:
    SeriesType();
    explicit SeriesType(bool type):useRawData(type){}
};

Eigen::MatrixXd toepliz(const std::vector<double> &a, const size_t taps)
{
    //Create toepliz matrix with measured waveform:
    const size_t N = a.size();
    Eigen::MatrixXd X(N+taps-1,taps);

    //Apply FIR coefficients exactly as obtained.
    vector<double> b(a.begin(),a.end());
    b.insert(b.end(),a.begin(),a.begin()+taps); //extend with data from first section
    for(size_t i=0;i<taps;i++){
        for(size_t j=0;j<(N+taps-1);j++){
            X(j,i)=b[j]; //assign column
        }
        rotate(b.begin(),b.end()-1,b.end()); //Move forward one unit
    }
    return X;
}

Eigen::MatrixXd toepliz_zeropadded(const std::vector<double> &a, const size_t taps)
{
    const size_t N = a.size();

    //Create toepliz matrix with measured waveform:
    Eigen::MatrixXd X(N+taps-1,taps);

    //Apply FIR coefficients exactly as obtained.
    vector<float> temp(a.begin(),a.end());
    temp.resize(N+taps); //put zero padding of taps digits at end of vector
    for(size_t i=0;i<taps;i++){
        for(size_t j=0;j<(N+taps-1);j++){
            X(j,i)=temp[j]; //assign column
        }
        rotate(temp.begin(),temp.end()-1,temp.end()); //Move forward one unit
    }
    return X;
}

bool OnTimeSignalSurvey::initializeOTSS(const std::vector<vectorCompStackedWaveform> &stationData,
                                        const HFieldOfLoop &loop,
                                        const size_t N_)
{
    ///"Initialize On-Time Signal Survey"

    ///VARIABLES
    /// stationData - contains location, coil orientation and data
    /// loop - see class description
    /// N_ - Number of samples per waveform

    //Initial error checks on field data:
    mLoop = unique_ptr<HFieldOfLoop>(new HFieldOfLoop(loop));
    mStationData = stationData;
    N=N_;
    if(N >1000000){ //Some big number
        throw runtime_error("N = "+to_string(N));
        return 0;
    }
    //Check that Data array lengths are consistent and correct
    size_t misfits = count_if(stationData.begin(),stationData.end(),[&N_](vectorCompStackedWaveform x){
            return x.data().size() != N_;});
    if (misfits !=0){
        auto it = find_if(stationData.begin(),stationData.end(),[&N_](vectorCompStackedWaveform x){
                return x.data().size() != N_;});
        string name = it->stationName();
        throw runtime_error(to_string(misfits)+" Stations are wrong length, including station: "+name);
        return 0;
    }
    return 1;
}

bool OnTimeSignalSurvey::process(const size_t taps)
{
    ///Calcualte a filter based on ideal on-time shape and use
    /// to clean on-time data by deconvolution.
    /// Measure time-channels during on-time.

    //error check:
    if(taps>N){
        throw runtime_error("taps ("+to_string(taps)+") can't be bigger than N ("+to_string(N)+")");
        return 0;
    }

    vector<double> mPrimary;
    mPrimary=weightedStacking();
    vector<double> firValues=calculateFilter(taps,mPrimary);
    applyToOriginalData(firValues);
    //    vector<pair<SVec, double> > result;
    //    vector<double> measurement = measureAmplitude(cleanStationStackedWaveforms);
    //package result together with station locations for easy plotting later
    //    size_t i=0;
    //    for(auto it = mStationData.begin();it!=mStationData.end();++it){
    //        SVec position = it->location();
    //        result.push_back(pair<SVec,double>(position,measurement[i]));
    //    }
    return 1;
}

optional< vector<pair<double,double>> > simpleRegression(Eigen::MatrixXd valPairs){
    ///Use least squares to scale first value to best fit second. Take residual and scale to measurements.
    ///valPairs is 3 X number of measurements:
    ///           valPairs(row,0)=it->MTWFRs[i].measurement;
    ///           valPairs(row,1)=it->MTWFRs[i].theoretical;
    ///           valPairs(row,2)=it->MTWFRs[i].weight;
    try {
        using namespace Eigen;
        VectorXd X = VectorXd::Ones(valPairs.rows());
        VectorXd Y = VectorXd::Ones(valPairs.rows());
        VectorXd X_ = VectorXd::Ones(valPairs.rows());
        VectorXd Y_ = VectorXd::Ones(valPairs.rows());
//        VectorXd W = VectorXd::Ones(valPairs.rows());

        //Ordinary least squares normal equations
        for(size_t i=0;i<valPairs.rows();i++){
            Y(i)=valPairs(i,0);
            X(i)=valPairs(i,1);
            Y_(i)=sqrt(abs(valPairs(i,2))) * valPairs(i,0);
            X_(i)=sqrt(abs(valPairs(i,2))) * valPairs(i,1); //Applying weights to both sides of equation
        }
        MatrixXd m = ((X_.transpose())*Y_)/((X_.transpose())*X_);
        VectorXd F=X*m; //"Fit"
        VectorXd R=Y-F; //"Residual"

        //Convert to a percentage
        for(size_t i=0; i<Y.rows(); i++){
            R(i)=100.0*(R(i))/(F(i));
            if(R(i)!=R(i)){
                //If NaN
                R(i)=0.0;
            }
        }

        //Include residuals
        vector<pair<double,double>> residual;
        for(size_t i=0; i<X.rows();i++){
            residual.push_back({F(i),R(i)});
        }
        return residual;
    }  catch (...) {
        return nullopt;
    }
}

std::optional< vector<stnProducts> > OnTimeSignalSurvey::specialProcessing(const vector<pair<size_t,size_t> > &timeGates)
{
    ///Calculate primary field at survey stations using "PolyLine" class.
    ///Measure average for each stacked waveform for each timeband.
    /// Perform regression to scale measurements to match corresponding theoretical field strength.
    ///Return residual values for each timeband.

    vector<double> primary;
    //Measure on raw field for each station (no deconvolution)
    auto x = getWindowedData(mStationData,timeGates,SeriesType::Raw());
    if(!x){
        throw runtime_error("problem in getWindowed Data function");
    }
    if(x->size() != mStationData.size()){
        throw runtime_error("Problem with getWindowedData. size mismatch "+to_string(x->size())+":"+to_string(mStationData.size()));
    }

    //Calculate theoretical field for all stations of the loop survey
    vector<vectorCompStackedWaveform>::iterator it = mStationData.begin();
    std::vector< pair<SVec, double> > L_TV; //Location, Theoretical Value
    for(;it!=mStationData.end();++it){
        SVec stnOrientation = it->orientation();
        SVec stnPosition = it->location();
        SVec localField = mLoop->getHField(stnPosition);
        double scaling = stnOrientation*localField; //calculated field strength along probe axis
        L_TV.push_back(pair<SVec, double>(stnPosition,scaling));
    }

    //Check that both sets cover the same locations (debug)
    if(!checkLocationsAreIdentical<vector< pair<SVec,std::vector<double>>>,vector<pair<SVec, double>>>(*x,L_TV)){
        throw runtime_error("locations not identical");
    }

    //Combine data from both lists into one structure
    vector< stnProducts > LMWTV; //"Location, Measurement, Weight, Theoretical Value"
    for(size_t i = 0; i<x->size();i++){
        SVec loc = (*x)[i].first;
        vector<double> measurement = (*x)[i].second;
        double theoreticalValue = L_TV[i].second;
        stnProducts temp;
        temp.location = {loc.x,loc.y};
        temp.elevation = loc.z;
        if(abs(theoreticalValue)<0.0001) {
            temp.MTWFRs.push_back({measurement.front(),theoreticalValue,0.0,0.0,0.0});
        }
        else{
            temp.MTWFRs.push_back({measurement.front(),theoreticalValue,1/theoreticalValue,0.0,0.0});
        }
        LMWTV.push_back(temp);
    }

    //Perform regression for each (timeband) measurement
    for(size_t i=0; i<LMWTV[0].MTWFRs.size();i++){
        Eigen::MatrixXd values(LMWTV.size(),3);
//        vector< pair<double,double> > valPairs;
        int row=0;
        for(auto it=LMWTV.begin();it!=LMWTV.end();++it){
            //**** Special test **** Weight theoretical to unity
//            double th = (it->MTWFRs[i].theoretical)*(it->MTWFRs[i].weight);
//            double meas = (it->MTWFRs[i].measurement)*(it->MTWFRs[i].weight);
            values(row,0)=it->MTWFRs[i].measurement;
            values(row,1)=it->MTWFRs[i].theoretical;
            values(row,2)=it->MTWFRs[i].weight;
//            valPairs.push_back({meas,th});
            row++;
        }
        auto regression = simpleRegression(values);
        if(!regression) {
            throw runtime_error("Problem in trivialRegression function");
        }
        //Put back into vector
        for(size_t j=0;j<regression->size();j++){
            LMWTV[j].MTWFRs[i].fit=regression->at(j).first;
            LMWTV[j].MTWFRs[i].residual=regression->at(j).second;
        }
    }

    if (LMWTV.empty()) return nullopt;
    return LMWTV;
}

vector<double> OnTimeSignalSurvey::weightedStacking()
{
    ///Estimate primary field waveform using survey data. Do this by combining the waveforms of all the stations weighting each
    ///according to threoretical field strength S^4 and the dotproduct of the coil orientation to the theoretical field.
    ///Store result in stackedWaveform vector.

    vector<vectorCompStackedWaveform>::iterator it = mStationData.begin();
    vector<double>::iterator it2;
    vector<double> stacked;
    stacked.assign(N,0.0);
    //Run through all stations of the loop survey
    for(;it!=mStationData.end();++it){
        SVec stnOrientation = it->orientation();
        SVec stnPosition = it->location();
        SVec localField = mLoop->getHField(stnPosition);
        double Scaling = stnOrientation*localField; //theoretical field strength
        vector<double> local = it->data();
        for_each(local.begin(),local.end(),[Scaling](double &x){
            x=x*pow(Scaling,4);
        });
        transform(stacked.begin(),stacked.end(),local.begin(),local.begin(),[](double &x, double &y){
            return x+y;
        });
    }
    //Normalize amplitude of measured primary field
    double amplitude=inner_product(stacked.begin(),stacked.end(),stacked.begin(),0.0);
    for_each(stacked.begin(),stacked.end(),[amplitude](double &i){
        i=(i/amplitude);});
    //Assign result
    return stacked;
}

vector<double> OnTimeSignalSurvey::calculateFilter(const size_t taps,const vector<double> mPrimary)
{
    ///Least squares calculation for FIR filter coefficients to best convert the stacked waveform of measured primary field
    /// to an ideal 4-cycle squarewave.

    //Error checks:
    if(mPrimary.empty()){
        throw runtime_error("stacked waveform empty. Can't do calculateFilter()");
    }
    if(mPrimary.size() != N){
        throw runtime_error("mismatch in sizes between stacked waveform and intended length");
    }
    if(taps > N){
        throw runtime_error("FIR filter set to have "+to_string(taps)+" taps on waveform length "+to_string(N));
    }
    if(taps % 2 != 0){
        throw runtime_error("taps in calculateFilter must be multiple of 2 not: "+to_string(taps));
    }

    //Create toepliz matrix with measured waveform:
    using namespace Eigen;
    //    MatrixXd X = toepliz_zeropadded(mPrimary, taps);
    MatrixXd X = toepliz(mPrimary, taps);
    VectorXd Y = VectorXd::Ones(N+taps);

    //Ideal response for Y
    for(size_t i=0;i<(N+taps);i++){
        if(i<(taps+(N/4)) && i>taps){
            Y(i)=1;
        }
        if(i<(taps+(3*N/4)) && i>(taps+(2*N/4))){
            Y(i)=-1;
        }
    }

    //    //Option #1 discover convolution filter to transform all 4-cycles
    //    JacobiSVD<MatrixXd> svd(X, ComputeThinU | ComputeThinV);
    //    cout << "Its singular values are:" << endl << svd.singularValues() << endl;
    //    cout << "Its left singular vectors are the columns of the thin U matrix:" << endl << svd.matrixU() << endl;
    //    cout << "Its right singular vectors are the columns of the thin V matrix:" << endl << svd.matrixV() << endl;
    //    MatrixXd m = svd.solve(Y);
    //    cout << "A least-squares solution of m*x = rhs is:" << endl << m << endl;

    //Option #2 ordinary least squares normal equations
    Y=(X.transpose())*Y;
    X=(X.transpose())*X;
    MatrixXd m = X.ldlt().solve(Y);

    //Transfer FIR coefficients to vector
    vector<double> firValues;
    for(size_t i=0;i<taps;i++){
        firValues.push_back(m(i));
    }
    return firValues;
}

std::optional< std::vector< std::pair<SVec,std::vector<double> > > > OnTimeSignalSurvey::getWindowedData(
        std::vector<vectorCompStackedWaveform> stationData,
        const vector<pair<size_t,size_t> > &timeGates,
        SeriesType type){
    ///Average for each timeGate for each station in stationData
    ///
    ///VARIABLES
    /// timeGates - series of timegates input by user (whole numbers for the start and end bins)
    /// SeriesType - either sample on the raw or clean (deconvolved) series
    ///RETURN VALUE
    /// result - locations (x,y,z) and averages for each timegate on each station

    //Package locations together with timegate values
    std::vector< std::pair<SVec,std::vector<double> > > result;

    //Error check:
    if(any_of(timeGates.begin(),timeGates.end(),[](pair<size_t,size_t> x){return x.second<x.first;})){
        throw runtime_error("A timeGate combination is incorrect");
        //return nullopt;
    }
    size_t N_=N;
    if(any_of(timeGates.begin(),timeGates.end(),[N_](pair<size_t,size_t> x){return x.second>N_;})){
        throw runtime_error("A timeGate index is above "+to_string(N_));
        //  return nullopt;
    }
    if(any_of(timeGates.begin(),timeGates.end(),[](pair<size_t,size_t> x){return x.first<0;})){
        throw runtime_error("A timeGate index is below zero");
        //return nullopt;
    }

    //Sum from start to end of each timegate range
    for(auto it=stationData.begin();it != stationData.end();++it){ //For each station
        vector<double>::const_iterator i;
        if(type.useRawData){
            i= it->begin();
        }
        else{
            i= it->mCleanData.begin();
        }
        vector<double> singleStnTimeGateVals;
        for(auto it2=timeGates.begin();it2 != timeGates.end();++it2){ //Sum across range specified
            size_t strt = it2->first;
            size_t fin = it2->second;
            const double sum = accumulate(i+strt,i+fin,0.0)/((double)(1+fin-strt)); //Average
            singleStnTimeGateVals.push_back(sum);

            //            tempCSV<<sum<<endl;
        }
        result.push_back(pair<SVec,vector<double> >(it->location(),singleStnTimeGateVals));
    }
    if (result.empty()){
        return nullopt;
    }
    else{
        return result;
    }
}

void OnTimeSignalSurvey::applyToOriginalData(const vector<double> firValues)
{
    ///Apply deconvolution filter to original field data

    //Transfer FIR coefficients to vector
    const size_t taps = firValues.size();
    //Transfer FIR coefficients to Eigen matrix
    using namespace Eigen;
    MatrixXd m(taps,1);
    for(size_t i=0;i<taps;i++){
        m(i,0)=firValues[i];
    }

    //Run through all stations of the loop survey
    vector<vectorCompStackedWaveform>::iterator it = mStationData.begin();
    for(;it!=mStationData.end();++it){
        vector<double> a= it->data();
        //Apply FIR coefficients exactly as obtained.
        MatrixXd X = toepliz_zeropadded(a, taps);
        VectorXd Y;
        Y=X*m;

        //Ideal response for Y
        vector<double> cleanStationStackedWaveforms;
        for(size_t i=(taps-1);i<(N+taps-1);i++){
            cleanStationStackedWaveforms.push_back(Y(i));
        }
        it->mCleanData=cleanStationStackedWaveforms;
    }
}
