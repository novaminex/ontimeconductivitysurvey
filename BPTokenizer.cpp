//---------------------------------------------------------------------------
#pragma hdrstop
#include "BPTokenizer.h"
#include <filesystem>
#include <string>
//---------------------------------------------------------------------------
namespace{
void removeTrailingWhiteSpace(std::vector<char> &c){
    //remove spaces, tabs CR LF
    for(size_t i=c.size()-1; i>=0; i--){
        if(!((c[i]=='\r')|| (c[i]=='\n')||(c[i]==' ')||(c[i]=='\t')||(c[i]=='\0'))){
            c.resize(i+1);
            return;
        }
    }
}
}
BPTokenizer::BPTokenizer(std::filesystem::path p) {
    m_good=false;
    std::ifstream infile(p);
    if(!infile.good())return;
    initialize(infile);
    m_good=true;
}

BPTokenizer::BPTokenizer(std::ifstream &fileStream) {
    m_good=false;

    size_t length;
    fileStream.seekg(0, std::ios::end); // go to the end
    length = fileStream.tellg(); // report location (this is the length)
    fileStream.seekg(0, std::ios::beg); // go back to the beginning
    fileData.resize(length+1);//add delimiter at end of file to make logic easier and faster
    char* buffer = &fileData[0];
    fileStream.read(buffer, length); // read the whole file into the buffer
    removeTrailingWhiteSpace(fileData);
    fileData.resize(fileData.size());
    fileStream.close();

    fileDataCopy.resize(length);//preserve the original file
    memcpy ( &fileDataCopy[0], &fileData[0], length);
    buffer[length]=char(0);
    //set default delimiters
    m_delimiters.resize(3);
    m_delimiters[0]=',';
    m_delimiters[1]=' ';
    m_delimiters[2]='\t';
    m_good=true;

}
void BPTokenizer::initialize(std::ifstream &fileStream) {
    int length;
    fileStream.seekg(0, std::ios::end); // go to the end
    length = (int)fileStream.tellg(); // report location (this is the length)
    fileStream.seekg(0, std::ios::beg); // go back to the beginning
    fileData.resize(length);//add delimiter at end of file to make logic easier and faster
    char* buffer = &fileData[0];
    fileStream.read(buffer, length); // read the whole file into the buffer
    removeTrailingWhiteSpace(fileData);
    fileData.resize(fileData.size());

    fileStream.close();

    fileDataCopy.resize(length);//preserve the original file
    memcpy ( &fileDataCopy[0], &fileData[0], length);
    buffer[length-1]=char(0);
    //set default delimiters
    m_delimiters.resize(3);
    m_delimiters[0]=',';
    m_delimiters[1]=' ';
    m_delimiters[2]='\t';
}

BPTokenizer::BPTokenizer() {


}

void BPTokenizer::Tokenize() {
    lineRecord currentLine;
    currentLine.lineStart = &fileData[0];
    size_t dataSize=fileData.size();
    size_t lastNewLine=0;
    for (size_t i = 0; i < dataSize; i++) {
        bool doLastLine=((i==dataSize-1)&&((i-lastNewLine)>1));

        if ((fileData[i] == '\n')||doLastLine) {
            lastNewLine=i;
            currentLine.lineEnd = &fileData[i - 1];
            if(i==dataSize-1)currentLine.lineEnd = &fileData[i];
            lines.push_back(currentLine);
            if(!doLastLine)fileData[i] = char(0);
            // null terminate by overwriting the end of line char
            if(i==(dataSize-1)){
                break;
            }
            currentLine.lineStart = &fileData[i + 1];

        }
    }
    int nlines = (int)lines.size();
    for (int i = 0; i < nlines; i++) {
        int nchar = (int)(lines[i].lineEnd - lines[i].lineStart) + 1;
        char *p = lines[i].lineStart;
        char *fieldStart = p;
        //		int lastDelimiter = -1;
        bool startedfield = false;
        for (int j = 0; j < nchar; j++) {
            if(isDelimiter(*p)){
                if (startedfield) {
                    lines[i].fields.push_back(fieldStart);
                    startedfield = false;
                    *p = char(0); // null terminate in place
                }
            }
            else {
                if (!startedfield) {
                    startedfield = true;
                    fieldStart = p;
                }
            }
            p++;
        }
        if (startedfield)
            lines[i].fields.push_back(fieldStart);

    }
}

bool BPTokenizer::getDouble(const size_t lineNumber, const size_t fieldNumber,
                            double &value)const {
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (false);
    if ((fieldNumber < 0) || (fieldNumber >= lines[lineNumber].fields.size()))
        return (false);
    try {
        value = std::stod(lines[lineNumber].fields[fieldNumber]);
        return (true);
    }
    catch (...) {
        return (false);
    }
}
bool BPTokenizer::getInt(const size_t lineNumber, const size_t fieldNumber,
                         int &value)const {
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (false);
    if ((fieldNumber < 0) || (fieldNumber > lines[lineNumber].fields.size()))
        return (false);
    try {
        value = std::atoi(lines[lineNumber].fields[fieldNumber]);
        return (true);
    }
    catch (...) {
        return (false);
    }
}

bool BPTokenizer::getString(const size_t lineNumber, const size_t fieldNumber,
                            std::string &string) const{
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (false);
    if ((fieldNumber < 0) || (fieldNumber > lines[lineNumber].fields.size()))
        return (false);
    try {
        if(fieldNumber>=lines[lineNumber].fields.size()){
            string="";
            return(true);
        }
        string = std::string(lines[lineNumber].fields[fieldNumber]);
        return (true);
    }
    catch (...) {
        return (false);
    }
}
bool BPTokenizer::getString(const size_t lineNumber, std::string &string)const {
    if ((lineNumber < 0) || (lineNumber >= lines.size()))return (false);
    std::string s("");
    auto nfields= lines[lineNumber].fields.size();
    for(size_t i=0; i<nfields; i++){
        std::string s1(lines[lineNumber].fields[i]);
        s+=s1;
        if(i<(nfields-1))s+=",";
    }
    string=s;
    return(true);
}


char BPTokenizer::getChar(const size_t lineNumber,size_t fieldNumber)const{
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (char(0));
    if ((fieldNumber < 0) || (fieldNumber > lines[lineNumber].fields.size()))
        return (char(0));
    try {
        if(fieldNumber>=lines[lineNumber].fields.size()){

            return(char(0));
        }
        std::string s= std::string(lines[lineNumber].fields[fieldNumber]);
        std::size_t found = s.find_first_not_of(" \t\0");
        return (s[found]);
    }
    catch (...) {
        return (false);
    }


}
bool BPTokenizer::split(size_t lineNumber, std::vector<std::string> &v) {
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (false);
    int nfields = static_cast<int>(lines[lineNumber].fields.size());
    v.resize(nfields);
    for (int i = 0; i < nfields; i++){
        v[i] = std::string(lines[lineNumber].fields[i]);
    }
    return(true);
}

size_t  BPTokenizer::nfields(size_t lineNumber) {
    if ((lineNumber < 0) || (lineNumber >= lines.size()))
        return (0);
    int nfields = static_cast<int>(lines[lineNumber].fields.size());
    return (nfields);
}

char* BPTokenizer::GetField(const size_t lineNumber, const size_t fieldNumber)const{

    return(lines[lineNumber].fields[fieldNumber]);
}

bool BPTokenizer::find(const std::string &StringtoFind,  size_t &lineNumber,  size_t &fieldNumber){
    m_searchLine=0;
    m_searchField=0;
    return(findNext(StringtoFind, lineNumber, fieldNumber));
}

bool BPTokenizer::findNext(const std::string &StringtoFind,  size_t &lineNumber,  size_t &fieldNumber){
    size_t searchField=m_searchField;
    for(size_t i=m_searchLine; i< lines.size(); i++){
        for(size_t j=searchField; j<lines[i].fields.size(); j++){
            searchField=0;
            std::string testit=std::string(lines[i].fields[j]);
            if(StringtoFind==testit){//std::string(lines[i].fields[j])){
                lineNumber=i;
                fieldNumber=j;
                m_searchLine=i;
                m_searchField=j+1;
                if(m_searchField>=lines[m_searchLine].fields.size()){

                    m_searchLine++;
                    m_searchField=0;
                }
                return(true);
            }
        }
    }
    return(false);
}

bool BPTokenizer::isCommentLine(size_t lineNumber){
    if(lineNumber>=lines.size())return(false);//off end of the file
    //check first field of line
    if(lines[lineNumber].fields.size()==0)return(true);//treat null line as a comment line
    if(std::strstr(lines[lineNumber].fields[0],m_commentString))return(true);
    return(false);
}
void BPTokenizer::setCommentString(const std::string &commentString){
    commentString.copy(m_commentString,64,0);//64 here is the same as the dimension of m_commentString
}

bool BPTokenizer::isDelimiter(char c){
    for(size_t i=0; i<   m_delimiters.size(); i++){
        if(c==m_delimiters[i])return(true);
    }
    return(false);
}

void BPTokenizer::setDelimiters(std::string delimiterList){
    m_delimiters.resize(delimiterList.size());
    for(size_t i=0; i<delimiterList.size(); i++){
        m_delimiters[i]=delimiterList[i];
    }
}

bool BPTokenizer::isLegit(size_t lineNumber,  size_t fieldNumber){
    if((lineNumber<0)||(lineNumber>=lines.size()))return(false);
    if((fieldNumber<0)||(fieldNumber>=nfields(lineNumber)))return(false);
    return(true);
}

bool BPTokenizer::addAfter( size_t lineNumber,  size_t fieldNumber,std::string &stringToAdd){
    //check to see if lineNumber and fieldNumber are legit
    if(!isLegit( lineNumber,   fieldNumber))return(false);
    m_fileInsertionList.push_back(std::make_tuple (lineNumber,fieldNumber,stringToAdd));
}
size_t BPTokenizer::getFieldAddress(size_t lineNumber, size_t fieldNumber){
    if(!isLegit(lineNumber,  fieldNumber))return(0);
    size_t offsetIntoFile=lines[lineNumber].fields[fieldNumber]-&fileData[0];
    return(offsetIntoFile+1);
}

size_t BPTokenizer::getNextFieldAddress(size_t lineNumber, size_t fieldNumber){
    if(!isLegit(lineNumber,  fieldNumber))return(0);
    fieldNumber++;
    if(fieldNumber>=lines[lineNumber].fields.size()){
        fieldNumber=0;
        lineNumber++;
        if(!isLegit(lineNumber,  fieldNumber))return(0);
    }
    size_t offsetIntoFile=lines[lineNumber].fields[fieldNumber]-&fileData[0];
    return(offsetIntoFile);
}
bool BPTokenizer::saveFile(std::ofstream &outFile){
    //write to file stream iserting the insertionList items at the correct places
    size_t nextCharToWrite=0;
    for(size_t i=0; i< m_fileInsertionList.size(); i++){
        //unpack the tuple
        size_t lineNumber=std::get<0>(m_fileInsertionList[i]);
        size_t fieldNumber=std::get<1>(m_fileInsertionList[i]);
        std::string insertionString=std::get<2>(m_fileInsertionList[i]);
        size_t insertionPoint=getNextFieldAddress(lineNumber,fieldNumber);

        for(size_t j=nextCharToWrite; j<insertionPoint; j++)outFile<<fileDataCopy[j];
        nextCharToWrite=insertionPoint;
        outFile<<insertionString;
    }
    for(size_t j=nextCharToWrite; j<fileDataCopy.size(); j++)outFile<<fileDataCopy[j];
    return(true);
}

/*
void  BPTokenizer::getDataTable(size_t startLine, size_t endLine, size_t ncols,std::vector<std::vector<std::string>> &dataTable){
    std::vector<std::vector<std::string>> rv;
    std::vector<std::string> temp(ncols);
    //preallocate
    size_t nrowsProvisional=endLine-startLine+1;
    dataTable.resize(nrowsProvisional);
    size_t ii=0;
    for(size_t i=startLine; i<=endLine; i++){
    size_t nf=nfields(i);
    if(nf!=ncols)continue;
    dataTable[ii].resize(ncols);

    for(size_t j=0; j<ncols; j++){
      dataTable[ii][j]=std::string(lines[i].fields[j]) ;
    }
        ii++;
}
}*/
bool stringOneOf(const std:: string &s, const std::vector<std::string> &slist){
    for(size_t i=0; i<slist.size(); i++)  {
        if(s==slist[i])return(true)  ;
    }
    return(false);
}
/*
bool  BPTokenizer::getDataTable(const std::vector<std::string> &dontIncludeThese,std::vector<std::vector<std::string>> &dataTable){
//find the number of columns in the output table
    dataTable.clear();
    for(auto it=m_titleIndex.begin(); it!= m_titleIndex.end(); it++){
        if(stringOneOf(it->first,dontIncludeThese))continue;
        std::vector<std::string> temp;
        dataTable.push_back(temp);

        if(!getColumnAsString(it->first, dataTable.back())){
           return(false);
        }
    }
   return(true);
}

*/
bool BPTokenizer::initDataTable(size_t titleLine, size_t lineStart, size_t lineEnd){
    m_titleLine=titleLine;
    size_t ncols=nfields(titleLine);
    m_dataTableStart=lineStart;
    m_dataTableEnd=lineEnd;
    m_dataTableNCols=ncols;
    m_titleLine=titleLine;
    if(ncols!=nfields(m_titleLine))return(false);
    std::string colstring;
    for(int i=0; i<ncols; i++){
        if(!getString(m_titleLine,i,colstring))return(false);
        transform(colstring.begin(), colstring.end(), colstring.begin(), ::tolower);
        m_titleIndex[colstring]=i;
    }

    //get rid of any rows in the table range that dont have the right number of columns..treat them as comments

    m_hitList.clear();
    for(size_t i=m_dataTableStart; i<=m_dataTableEnd; i++){
        if(nfields(i)==ncols)m_hitList.push_back(i);
    }
}

bool BPTokenizer::getColumnAsDouble(std::string colName, std::vector<double> &values)const{
    transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
    auto it=m_titleIndex.find(colName);
    if(it==m_titleIndex.end()){
        values.clear();
        return(false);
    }
    size_t col=it->second;
    values.resize(m_hitList.size());

    for(size_t i=0; i<m_hitList.size(); i++){
        if(!getDouble(m_hitList[i],col,values[i])){
            return(false);
        }
    }
    return(true);
}

bool BPTokenizer::getColumnAsDouble(std::vector<std::string> colNames, std::vector<double> &values)const{
    for(size_t k=0; k<colNames.size(); k++){
        transform(colNames[k].begin(), colNames[k].end(), colNames[k].begin(), ::tolower);
        auto it=m_titleIndex.find(colNames[k]);
        if(it!=m_titleIndex.end()){
            return(getColumnAsDouble(colNames[k],values));
        }
    }
    return(false);

}

bool BPTokenizer::getColumnAsString(std::string colName, std::vector<std::string> &values)const{
    transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
    auto it=m_titleIndex.find(colName);
    if(it==m_titleIndex.end()){
        values.clear();
        return(false);
    }
    size_t col=it->second;
    values.resize(m_hitList.size());

    for(size_t i=0; i<m_hitList.size(); i++){
        if(!getString(m_hitList[i],col,values[i])){
            return(false);
        }
    }
    return(true);
}
bool BPTokenizer::getColumnAsInt(std::string colName, std::vector<int> &values)const{
    transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
    auto it=m_titleIndex.find(colName);
    if(it==m_titleIndex.end()){
        values.clear();
        return(false);
    }
    size_t col=it->second;
    values.resize(m_hitList.size());

    for(size_t i=0; i<m_hitList.size(); i++){
        if(!getInt(m_hitList[i],col,values[i])){
            return(false);
        }
    }
    return(true);
}


bool BPTokenizer::columnTitleExists(std::string colName){
    transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
    auto it=m_titleIndex.find(colName);
    return(it!=m_titleIndex.end());
}


bool BPTokenizer::getColumnAsChar(std::string colName, std::vector<char> &values)const{
    transform(colName.begin(), colName.end(), colName.begin(), ::tolower);
    auto it=m_titleIndex.find(colName);
    if(it==m_titleIndex.end()){
        values.clear();
        return(false);
    }
    size_t col=it->second;
    values.resize(m_hitList.size());

    for(size_t i=0; i<m_hitList.size(); i++){
        values[i]=getChar(m_hitList[i],col);
    }
    return(true);
}




