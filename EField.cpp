//---------------------------------------------------------------------------

#pragma hdrstop

#include "EField.h"

#include <vector>
#include <cmath>
#include <SVec.h>
#include "math.h"

//---------------------------------------------------------------------------


double aarcsinh(double x){
   return( log(x+sqrt(x*x+1.0)));
 }

 //////////////////  Methods of VectorPotentialofPolyLine

 SVec VectorPotentialofPolyLine::VectorPotentialofSegment(const SVec &start, const SVec &end, const SVec &location) const{
   //compute x and y: the Distance along the wire segment and perpendicular offset from that direction to the field point
   SVec segmentDirection=end-start;
   double L=segmentDirection.Length();
   segmentDirection.Normalize();
   SVec R2Vector=location-start;
   SVec R1Vector=location-end;

   double x=R2Vector*segmentDirection;//dot product to get projected Distance in x
   SVec perpendicularProjecionPoint=start+segmentDirection*x;
   SVec transverseVector=location-perpendicularProjecionPoint;
   double y=transverseVector.Length();
//   double R2=R2Vector.Length();
//   double R1=R1Vector.Length();
   double rv;
   double signOfX=1.0;
   if(x<0.0)signOfX=-1.0;

   if((x<0)||(x>=L)){
	 double alpha=y/x;
     double gamma=L/x;

	 if(y/L>1.0e-5){
		rv=signOfX*log((sqrt((1.0-gamma)*(1.0-gamma)+alpha*alpha)+gamma-1.0)/(sqrt(1.0+alpha*alpha)-1.0));
	 }
	 else{  //do asymptotic approximation for small y                           8
	   rv=signOfX*log(sqrt((1.0+alpha*alpha)/((1.0-gamma)*(1.0-gamma)+alpha*alpha)));
	 }
   }
   else{ //x is between (0,L)
	 rv=aarcsinh(((L-x)*sqrt(1.0+x*x/(y*y))+x*sqrt(1.0+(L-x)*(L-x)/(y*y)))/y);

   }

   return segmentDirection*(1.0e-7*rv);        //vector potential is everywhere in the same direction as the segment
}

VectorPotentialofPolyLine::VectorPotentialofPolyLine(const std::vector<SVec> PolyLine){
   m_PolyLine=PolyLine;
   m_delta=1.0e-6;//  for computing curl by differencing
 }

 SVec VectorPotentialofPolyLine::getVectorPotential(const SVec &location) const{
   SVec sum(0.0,0.0,0.0);
   for(int i=1; i<m_PolyLine.size(); i++){
	 sum=sum+VectorPotentialofSegment(m_PolyLine[i],m_PolyLine[i-1],location);
   }
  return(sum);
}

SVec VectorPotentialofPolyLine::curlE(const SVec &location){
  //make curl by differencing

  SVec dx(m_delta,0.0,0.0);
  SVec dy(0.0,m_delta,0.0);
  SVec dz(0.0,0.0,m_delta);

  SVec Vpx= getVectorPotential(location+dx);
  SVec Vpmx= getVectorPotential(location-dx);
  SVec Vpy= getVectorPotential(location+dy);
  SVec Vpmy= getVectorPotential(location-dy);
  SVec Vpz= getVectorPotential(location+dz);
  SVec Vpmz= getVectorPotential(location-dz);

  SVec dFdy=(Vpy-Vpmy)/(2*m_delta);
  SVec dFdx=(Vpx-Vpmx)/(2*m_delta);
  SVec dFdz=(Vpz-Vpmz)/(2*m_delta);

  //dFz/dy-dFy/dz xhat    + dFx/dz - dFz/dx  yhat +  Fy/dx - dFx/dy
  SVec rv(dFdy.z-dFdz.y, dFdz.x-dFdx.z ,  dFdx.y-dFdy.x );
  return(rv);
}


//////////////////  Methods of HFieldOfLoop



HFieldOfLoop::HFieldOfLoop(const std::vector<SVec> PolyLine, bool closedloop){
   m_PolyLine=PolyLine;
   m_closedLoop= closedloop;
 }


 SVec HFieldOfLoop::getHField( const SVec &location) {

    const SVec *pointa, *pointb;
    const SVec *pointp;
    SVec ravec, rbvec, yvec, abunitvec, localhvec, temphvec;

    int nVertex = static_cast<int>(m_PolyLine.size());

	// Check if the first and last loop points are the same
	{
		// Subtract the first point from the last point
        SVec delta = m_PolyLine.back() - m_PolyLine.front();

        // If the Distance is less than some epsilon value, then
		// we ignore the last point.
		if (delta.Length() < 0.000001) {
			nVertex--;
		}
	}

    // vector<SVec> &m_PolyLine = m_PolyLine_const;

	pointp = &location;
	int nsegments = nVertex;

	if (!m_closedLoop) {
		nsegments--;
	}

	for (int iVertex = 0; iVertex < nsegments; iVertex++) {
		pointa = &m_PolyLine[iVertex];

		if (iVertex == nVertex - 1)
			pointb = &m_PolyLine[0]; // point back to the first one
		else
			pointb = &m_PolyLine[iVertex + 1];

		// make vectors
		ravec = *pointa - *pointp;
		rbvec = *pointb - *pointp;
		abunitvec = *pointb - *pointa;
        abunitvec.Normalize();
		double ra = ravec.Length();
		double rb = rbvec.Length();

		// find a and b values by dot prodct of r vector with [a,b] unit vector
		double a = ravec * abunitvec;
		double b = rbvec * abunitvec;

		double sa;

		if (a < 0.0)
			sa = -1.0;
		else
			sa = 1.0;

		// Following cases necessary to control and symetrize error

		double chi;

		if (fabs(a) < fabs(b)) {
			yvec = ravec - (abunitvec * a);
			double y = yvec.Length();

			if ((y / ra < 0.040) && (a * b > 0.0))
				// threshold fro asymptotic expansion adjusted for single precision arithmatic
			{
				// use asymptotic expansion for small y to fourth order
				double asq = a * a;
				double bsq = b * b;
				double ysq = y * y;
				chi = sa * 0.5 * y * ((1.0 - 0.75 * ysq / asq) / asq -
					(1.0 - 0.75 * ysq / bsq) / bsq);
			}
			else
				chi = (b / rb - a / ra) / y;
		}
		else // abs(b)<abs(a)
		{
			// compute y vector
			yvec = rbvec - (abunitvec * b);
			double y = yvec.Length();

			if ((y / rb < 0.040) && (a * b > 0.0))
				// threshold fro asymptotic expansion adjusted for single precision arithmatic
			{
				// use asymptotic expansion for small y to fourth order
				double asq = a * a;
				double bsq = b * b;
				double ysq = y * y;
				chi = sa * 0.5 * y * ((1.0 - 0.75 * ysq / asq) / asq -
					(1.0 - 0.75 * ysq / bsq) / bsq);
			}
			else
				chi = (b / rb - a / ra) / y;
		}

		// get vector direction of the field by the cross product of the y vector with the ab vector

		localhvec = yvec / abunitvec;

		double sum = localhvec.Length();

			sum = chi / sum;

		localhvec = localhvec * sum;

		// we have in localhvec the contribution from the iseg wire segment
		temphvec = temphvec + localhvec;
	} // End: for (int iVertex = 0; iVertex < nsegments; iVertex++)

	return(temphvec * 0.07957747155); // constant is 1/(4*pi)
}


