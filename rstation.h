#ifndef RSTATION_H
#define RSTATION_H

#include <string>

#include "SVec.h"

class RStation
{
private:
    SVec m_location;
    std::string m_stationName;
    RStation();
public:
    inline const SVec &location() const
    {
        return m_location;
    }

    inline const std::string &stationName() const
    {
        return m_stationName;
    }

    RStation(SVec location, std::string stationName):m_location(location),m_stationName(stationName){};
};

#endif // RSTATION_H
