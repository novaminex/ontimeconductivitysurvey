#ifndef SVec_H
#define SVec_H

/**
 * <code>SVec</code> Smart vector class to be used for linear-
 * algebra style x,y,z manipulations
 *
 * @Author TBag
 */
#include <iostream>
#include <vector>

class SVec4
{
public:
    SVec4() = default;
    SVec4(double r_i, double g_i, double b_i, double a_i);
    double r,g,b,a;
    std::array<float,4> toFloat4() const;
    std::vector<unsigned char> toUnsignedCharVec() const;
    static SVec4 fromArray(const std::array<float,4>& array);
    SVec4& operator=(const SVec4& other);


};

class SVec
{
public:
    double x,y,z;
    SVec();
    SVec(const double& x_i, const double& y_i, const double& z_i);
    SVec(const std::array<float,3>& array);
    SVec(const std::array<double,3>& array);
    //SVec(double&& x_i, double&& y_i, double&& z_i);
    SVec(const double& dip, const double& dipdir);
    SVec(const SVec& other);
    SVec(SVec&& other);


    void Normalize();
    double NormalizeL();
    SVec Normalized() const;
    SVec& operator =(const SVec& second);
    SVec& operator =(SVec&& second);
    SVec& operator =(const std::array<float,3>& second);
    SVec& operator =(const std::array<double,3>& second);
    bool operator ==( const SVec& second ) const;
    bool equals(const SVec& other, double tolerance = 0.00001) const;
    bool operator !=( const SVec& second ) const;
    SVec operator +( const SVec& second ) const;
    SVec& operator+=(const SVec& second){//
        *this = *this + second;
        return *this;
    }
    SVec operator-(const SVec& second) const;
    SVec& operator-=(const SVec& second);
    double operator *(const SVec& second ) const;  // dot product
    SVec operator /(const SVec& second ) const;  // cross product
    SVec operator /(const double& c) const;
    SVec operator *(const double& c) const{//
       return SVec(x*c, y*c, z*c);
    }
    SVec& operator *=(double c){//
        *this = *this * c;
        return *this;
     }
    SVec& operator /=(double k);
    inline double Length() const{
         return std::sqrt(x*x+y*y+z*z);
    }

    double Distance(const SVec &otherPoint)const{
        double dx=x-otherPoint.x;
        double dy=y-otherPoint.y;
        double dz=z-otherPoint.z;
        return(std::sqrt(dx*dx+dy*dy+dz*dz));
    }//
    SVec Rotate(const SVec &rotationAxis, double radAngle);

    void RotateSelf(const SVec &rotationAxis, double radAngle);
    friend double operator*(double lhs, const SVec& rhs);

    static SVec fromArray(const std::array<float,3>& array);
    static SVec fromArray(const std::array<double,3>& array);
    std::array<float, 3> toFloat3() const;
    std::array<double,3> toDouble3() const;
    double angle(const SVec& other) const;



};

std::ostream &operator<< (std::ostream &os, const SVec &v);

#endif // SVec_H
