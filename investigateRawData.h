#ifndef INVESTIGATERAWDATA_H
#define INVESTIGATERAWDATA_H
///Functions for measuring standard devitaion of the raw data. Originally on mainwindow, but got to cluttered.

#include <filesystem>
void standardDeviationOfSTKFiles(std::vector<std::pair<size_t,size_t> > timeGates,std::filesystem::path toCSVRaw);
void standardDeviationOfStackShape(const std::pair<size_t,size_t> timeRange,const std::filesystem::path toCSVRaw);

#endif // INVESTIGATERAWDATA_H
