#include <vector>
#include <exception>
#include <stdexcept>
#include <string>
#include <numeric>
#include <functional>
#include <algorithm>
#include <optional>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
#include<fstream>

#include "sm24_stk_reader.h"
#include "BPTokenizer.h"
#include "investigateRawData.h"
#include "SVec.h"

using namespace Eigen;
using namespace std;
namespace fs = std::filesystem;

size_t N=240000; //time series is 240000 samples for an sm24 stk file.

class CSVRowInfo{
    ///Used in standardDeviationOfStackShape routine.
public:
    size_t row;
    fs::path stkFile;
    string UTME,UTMN,Elevation;
    size_t group; //same orientation and location
    pair<size_t,size_t> timeBand; //same for all instances
    size_t rowInEigenMatix=99999999; //Store data in Matrix to make this class lightweight

    CSVRowInfo(size_t row,fs::path file,string UTME,string UTMN,string Elevation,size_t group,pair<size_t,size_t> band):
        row(row),stkFile(file),UTME(UTME),UTMN(UTMN),Elevation(Elevation),group(group),timeBand(band)
    {};
    CSVRowInfo(){};
    //private:
};

bool sameGroup(CSVRowInfo i, CSVRowInfo j) {
    if (i.group == j.group){
        return 1;
    }
    else{
        return 0;
    }
}

bool compareGroupNumber(CSVRowInfo i, CSVRowInfo j){
    return (i.group<j.group);
}

bool validEntry(CSVRowInfo i){
    return (i.rowInEigenMatix != 99999999);
}

MatrixXd calculateSD(vector<CSVRowInfo>::iterator a,vector<CSVRowInfo>::iterator b){
    ///A special standard deviation routine for standardDeviationOfStackShape

    //Load file data into workspace martix
    const size_t rows=b-a+1;
    MatrixXd workspace(rows,N+1);
    for(size_t i=0; i<rows; i++){
        SM24_stk_reader reader;
        reader.initialize((a+i)->stkFile);
        vector<double> x = reader.getStackedSeries();
        workspace(i,0)=0; //This is the place for the average
        workspace.block(i,1,1,N)=Map<Matrix<double, Dynamic, Dynamic,RowMajor> >(x.data(),1,x.size());
    };

    //Move timeBand section to left side for easier work
    const size_t c1 = 1+a->timeBand.first;
    const size_t c2 = 1+a->timeBand.second;
    const size_t Nb = c2-c1;
    workspace.block(0,1,rows,Nb) = workspace.block(0,c1,rows,Nb);

    //Put Average of timeBand in first column, subtract and then divide each row by that number. Multiply by 100% for relative variation
    {
        workspace.block(0,0,rows,1) = workspace.block(0,1,rows,Nb).rowwise().mean(); //Average of timesereis in 1st column
        MatrixXd rowAvgs = (workspace.leftCols(1))*MatrixXd::Ones(1,Nb);
        workspace.block(0,1,rows,Nb)=100.0*((workspace.block(0,1,rows,Nb)-rowAvgs).cwiseQuotient(rowAvgs));
        workspace.block(0,0,rows,Nb) = workspace.block(0,1,rows,Nb); //Overwrite average column
    }

    //Average each of the 10 sections of timeband
    double fin = Nb;
    vector<pair<size_t,size_t>> bands;
    for(size_t i=0;i<10;i++){
        double x = i;
        size_t bandStrt= floorl(fin*(x/10.0));
        size_t bandFin= floorl(fin*((x+1.0)/10.0));
        bands.push_back({bandStrt,bandFin});
        workspace.block(0,i,rows,1)=workspace.block(0,bandStrt,rows,1+bandFin-bandStrt).rowwise().mean();//Place average in consecutive columns on left side of matrix
    }

    //Reduce size of matrix to only the 10 averages
    MatrixXd bandAvgs = workspace.block(0,0,rows,10);

    //SD of each column
    MatrixXd EX(1,10);
    EX=bandAvgs.colwise().mean();
    MatrixXd EX2(1,10);
    EX2=bandAvgs.colwise().squaredNorm()/((double)rows);
    MatrixXd SD=(EX2.array()-(EX.cwiseProduct(EX)).array()).array().sqrt();

    return SD;
}

void standardDeviationOfStackShape(const pair<size_t,size_t> timeRange,const fs::path toCSVRaw){
    ///Capture the change in waveform shape for groups of repeat measurements of same
    /// sensor orientation and location on survey course.
    ///
    /// timeRange - Start and end indices into stacked waveform
    /// toCSVRaw  - lists all the data series files (STK format) by group number
    ///
    /// Routine produces a csv with 10 intervals spanning the timeRange and SD of each
    /// expressed as a percentage

    //error checks
    if(timeRange.second-timeRange.first<10){
        throw runtime_error("standardDeviationOfStackShape wrong timeRange: "+to_string(timeRange.first)+" "+to_string(timeRange.second));
    }
    if(!fs::exists(toCSVRaw)){
        throw runtime_error("standardDeviationOfStackShape toCSVRaw : "+toCSVRaw.generic_string());
    }

    fs::path toStackedFolder = toCSVRaw.parent_path().parent_path();
    toStackedFolder.concat("\\stacked\\");

    //Import info about stk files and load into CSVRowInfo objects
    BPTokenizer stnDataList(toCSVRaw);
    stnDataList.Tokenize();
    vector<SVec> coordinates;
    vector<CSVRowInfo> csvIn;
    size_t totalGroups = 0; //Each is a location and component combination
    //    for(size_t i=stnDataList.nlines()-20; i<stnDataList.nlines();  i++){
    for(size_t i=1; i<stnDataList.nlines(); i++){
        string filename;
        stnDataList.getString(i,0,filename);
        filename.assign(filename.begin()+1,filename.begin()+filename.size()-1);
        fs::path stkFile = toStackedFolder;
        stkFile.append(filename);
        stkFile.concat(".stk");
        if(!fs::exists(stkFile)){
            throw runtime_error("standardDeviationOfStackShape stkFile doesn't exist : "+stkFile.generic_string());
        }
        int temp;
        stnDataList.getInt(i,4,temp);
        string UTME;
        stnDataList.getString(i,1,UTME);
        string UTMN;
        stnDataList.getString(i,2,UTMN);
        string Elevation;
        stnDataList.getString(i,3,Elevation);
        size_t group=temp; //Same group means like measurements: same component and location.
        //error check:
        if(group != group ){
            throw runtime_error("Nan detected in loop file");
        }
        if (totalGroups<group){
            totalGroups=group;
        }
        csvIn.push_back(CSVRowInfo(i,stkFile,UTME,UTMN,Elevation,group,timeRange));
    }

    MatrixXd M(totalGroups,10); //Holds the SD calculated values

    //Calucation of SD
    for(size_t i = 0;i<totalGroups;i++){
        //        for(size_t i = 0;i<5;i++){//
        auto bound = partition(csvIn.begin(), csvIn.end(),[&i](CSVRowInfo x){return x.group == i+1;});
        MatrixXd gates = calculateSD(csvIn.begin(),bound);
        M.block(i,0,1,10)=gates;
        for_each(csvIn.begin(),bound,[&i](CSVRowInfo &x){x.rowInEigenMatix=i;});
    }

    //No need to repeat entries for same location/orientation
    auto it = partition(csvIn.begin(),csvIn.end(),validEntry);
    csvIn.resize(distance(csvIn.begin(),it));
    sort(csvIn.begin(),csvIn.end(),compareGroupNumber);
    auto it2 = unique(csvIn.begin(),csvIn.end(),sameGroup);
    csvIn.resize(distance(csvIn.begin(),it2));

    //Make human readable spreadsheet
    fs::path toSheetOut = toCSVRaw.parent_path().parent_path();
    toSheetOut.concat("\\infoOnRawData.csv");
    ofstream csv_out(toSheetOut.generic_string(),'w');
    //Write contents of original csv header then new columns
    csv_out<<"Sample file from group,UTME,UTMN,Elevation,Group,gate1,gate2,gate3,gate4,gate5,gate6,gate7,gate8,gate9,gate10"<<endl;
    for_each(csvIn.begin(),csvIn.end(),
             [&csv_out,&M](CSVRowInfo x){
        csv_out<<x.stkFile.stem()<<",";
        csv_out<<x.UTME<<",";
        csv_out<<x.UTMN<<",";
        csv_out<<x.Elevation<<",";
        csv_out<<x.group<<",";
        for (size_t j=0;j<10;j++){
            csv_out<<M(x.rowInEigenMatix,j)<<",";
        }
        csv_out<<endl;
    });
    csv_out.close();
}

void standardDeviationOfSTKFiles(vector<pair<size_t,size_t> > timeGates,fs::path toCSVRaw){
    ///Run through CSV list of raw binary stacked waveforms and generate standard deviation values
    ///for repeated measurements of same component and location.
    ///Groups of repeat measurements have an identification number in "toCSVRaw".
    ///SD is calculated by first averaging each time series within the timeGate indices to produce one value per file,
    /// then determining the SD of these values.
    /// This is done for every timeGate pair and the result recorded back to the csv.

    fs::path toStackedFolder = toCSVRaw.parent_path().parent_path();
    toStackedFolder.concat("\\stacked\\");

    BPTokenizer stnDataList(toCSVRaw);
    stnDataList.Tokenize();
    vector<SVec> coordinates;
    vector<pair<size_t,pair< size_t,vector<pair<double,double>>>>> vpRpGvpAS; //"vector of pairs of Row number and pairs of Group number, vector of pairs of Average and Standard deviation for each timegate" TO DO: refactor as a structure
    int totalGroups = 0;
    //    for(size_t i=stnDataList.nlines()-20; i<stnDataList.nlines();  i++){
    for(size_t i=1; i<stnDataList.nlines(); i++){
        string filename;
        stnDataList.getString(i,0,filename);
        filename.assign(filename.begin()+1,filename.begin()+filename.size()-1);
        fs::path stkFile = toStackedFolder;
        stkFile.append(filename);
        stkFile.concat(".stk");
        int temp;
        stnDataList.getInt(i,4,temp);
        size_t group=temp; //Same group means like measurements: same component and location.
        //error check:
        if(group != group ){
            throw runtime_error("Nan detected in loop file");
        }
        if (totalGroups<group){
            totalGroups=group;
        }
        SM24_stk_reader reader;
        reader.initialize(stkFile);
        vector<double> x = reader.getStackedSeries();

        vector<pair<double,double>> vpAS; //"pairs of Average and Standard deviation for each timegate"
        for(size_t j=0;j<timeGates.size();j++){
            double avg = accumulate(x.begin()+timeGates[j].first,x.begin()+timeGates[j].second,0.0);
            avg/=((double)(timeGates[j].second-timeGates[j].first));
            vpAS.push_back({avg,0.0}); //Calculate SD later
        }
        vpRpGvpAS.push_back({i,{group,vpAS}});
    }

    //Calculate SD for each group (same location and component)
    for(int i = 1;i<=totalGroups;i++){
        auto bound = partition(vpRpGvpAS.begin(), vpRpGvpAS.end(),[&i](auto x){
            return x.second.first == i;
        });
        //For each timegate
        size_t Ntg=vpRpGvpAS.front().second.second.size();
        for(size_t tg=0;tg<Ntg;tg++){
            //Get SD for values of Timegate
            double X2avg=0.0;
            double Xavg=0.0;
            double N=0.0;
            //Go through each measurement of the group
            for(auto it2 = vpRpGvpAS.begin();it2!=bound;++it2){
                Xavg+=(it2->second.second[tg].first);
                X2avg+=(it2->second.second[tg].first)*(it2->second.second[tg].first);
                N=N+1.0;
            }
            X2avg/=N;
            Xavg/=N;
            double a = X2avg;
            double b = Xavg*Xavg;
            double c = a-b;
            double SD = sqrtl(c);
            //Assign
            for(auto it2 = vpRpGvpAS.begin();it2!=bound;++it2){
                it2->second.second[tg].second=SD;
            }
        }
    }
    sort(vpRpGvpAS.begin(),vpRpGvpAS.end(),[](auto x, auto y){return (x.first<y.first);});
    //Place back into spreadsheet
    for(auto it=vpRpGvpAS.begin();it!=vpRpGvpAS.end();++it){
        //Assign
        int row = (it-vpRpGvpAS.begin())+1;
        int columnStart = 5;
        for(auto it2 = it->second.second.begin();it2!=it->second.second.end();++it2){
            string AVG = to_string(it2->first);
            string SD = to_string(it2->second);
            stnDataList.addAfter(row,columnStart+1,AVG);
            stnDataList.addAfter(row,columnStart+2,SD);
            columnStart=columnStart+2;
        }
    }

    fs::path toSheetOut = toCSVRaw.parent_path().parent_path();
    toSheetOut.concat("\\infoOnRawData.csv");
    ofstream csv_out(toSheetOut.generic_string());
    //Write contents of original csv header then new columns
    for(size_t k=0;k<5;k++){ //Original content
        string item;
        stnDataList.getString(0,k,item);
        csv_out<<item<<",";
    }
    for(size_t k=0;k<vpRpGvpAS.front().second.second.size();k++){
        csv_out<<"AVG"<<to_string(k+1)<<",SD"<<to_string(k+1)<<","<<endl;
    }
    for(auto it=vpRpGvpAS.begin();it!=vpRpGvpAS.end();++it){
        //Write one line in csv
        int row = (it-vpRpGvpAS.begin())+1;
        int columnStart = 5;
        for(size_t k=0;k<5;k++){ //Original data content
            string item;
            stnDataList.getString(row,k,item);
            csv_out<<item<<",";
        }
        //csv_out<<"newLine";
        std::streamsize ss = std::cout.precision();
        for(auto it2 = it->second.second.begin();it2!=it->second.second.end();++it2){ //New data content
            double AVG = it2->first;
            double SD = it2->second;
            csv_out.precision (ss);
            csv_out<< scientific;
            csv_out<<AVG<<","<<SD<<",";
            columnStart=columnStart+2;
        }
        csv_out<<endl;
    }
    csv_out.close();
}
