#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "vectorcompstackedwaveform.h"
#include "EField.h"
#include "sm24_stk_reader.h"
#include "BPTokenizer.h"

#include <QFileDialog>
#include <QApplication>

#include <exception>
#include <stdexcept>
#include <string>
#include <numeric>
#include <functional>
#include <filesystem>
#include <algorithm>
#include <optional>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include<QDebug>

#include "ontimesignalsurvey.h"
#include "investigateRawData.h"

using namespace Eigen;
using namespace std;
using namespace OnTimeSignalSurveyTools;
using namespace boost::property_tree;
namespace fs = std::filesystem;

//Values used in initial stage to reorganize the files
vector< fs::path > listSTKfiles; //From folder containing survey data stk files
fs::path pathToTEM_File;
fs::path pathToGPS_File;

//Changes are made to these default values using INI file
QString accurateGPS="D:\\GPS\\Feb15.csv";
vector<fs::path> temDataDailyFolders = {"D:/TEM Data/Dec01",
                                        "D:/TEM Data/Dec02",
                                        "D:/TEM Data/Dec03",
                                        "D:/TEM Data/Dec04",
                                        "D:/TEM Data/Dec05",
                                        "D:/TEM Data/Dec06",
                                        "D:/TEM Data/Dec08",
                                        "D:/TEM Data/Dec09",
                                        "D:/TEM Data/Dec10",
                                        "D:/TEM Data/Dec11",
                                        "D:/TEM Data/Dec12",
                                        "D:/TEM Data/Dec13",
                                        "D:/TEM Data/Dec14",
                                        "D:/TEM Data/Dec15",
                                        "D:/TEM Data/Dec16",
                                        "D:/TEM Data/Dec17",
                                        "D:/TEM Data/Dec18",
                                        "D:/TEM Data/Feb_03_A",
                                        "D:/TEM Data/Feb03_B",
                                        "D:/TEM Data/Feb04_A",
                                        "D:/TEM Data/Feb04_B",
                                        "D:/TEM Data/Feb05_A",
                                        "D:/TEM Data/Feb05_B",
                                        "D:/TEM Data/Feb06_A",
                                        "D:/TEM Data/Feb06_B",
                                        "D:/TEM Data/Feb07_A",
                                        "D:/TEM Data/Feb08_A",
                                        "D:/TEM Data/Feb08_B",
                                        "D:/TEM Data/Feb09_A",
                                        "D:/TEM Data/Feb09_B",
                                        "D:/TEM Data/Feb10_A",
                                        "D:/TEM Data/Feb10_B",
                                        "D:/TEM Data/Feb11",
                                        "D:/TEM Data/Feb12",
                                        "D:/TEM Data/Feb13",
                                        "D:/TEM Data/Feb14",
                                        "D:/TEM Data/Jan15",
                                        "D:/TEM Data/Jan16",
                                        "D:/TEM Data/Jan17",
                                        "D:/TEM Data/Jan18",
                                        "D:/TEM Data/Jan19",
                                        "D:/TEM Data/Jan20",
                                        "D:/TEM Data/Jan21",
                                        "D:/TEM Data/Jan22",
                                        "D:/TEM Data/Nov20",
                                        "D:/TEM Data/Nov21",
                                        "D:/TEM Data/Nov22",
                                        "D:/TEM Data/Nov23",
                                        "D:/TEM Data/Nov24",
                                        "D:/TEM Data/Nov25",
                                        "D:/TEM Data/Nov26",
                                        "D:/TEM Data/Nov27",
                                        "D:/TEM Data/Nov28",
                                        "D:/TEM Data/Nov29",
                                        "D:/TEM Data/Nov30"
                                       };
fs::path pathToLoopFolder = "D:/TEM Data/LoopFiles/";
string dataFolder = "D:/TEM Data/";

//Report progress and errors
ofstream debugList;//("D:\\TEM Data\\completed.txt");
ofstream debug;//("D:\\TEM Data\\debug.txt");

enum component {X,Y,Z};
enum loopNumber {Loop_1=1,Loop_2,Loop_3};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void checkForNode(ptree in, string name){
    boost::optional<ptree&> child = in.get_child_optional(name);
    if( !child )
    {
        throw runtime_error("Missing node: "+name);
    }
}

ptree readIniFile(std::string iniFileName)
{
    //Error Check
    filesystem::path f(iniFileName);
    if (!exists(f)){
        throw runtime_error("Cannot open ini file: "+iniFileName);
    }

    ptree temp;
    boost::property_tree::ini_parser::read_ini(iniFileName, temp);
    checkForNode(temp,"Folders.rootFolder");

    //INI format should look like:
    //    [Folders]
    //    rootFolder="D:"

    return temp;
}

void assignPaths(ptree iniFile){
    ///Add root folder to beginning of all needed survey files

    string rootPath = iniFile.get<string>("Folders.rootFolder");
    accurateGPS.replace(accurateGPS.indexOf(QString("D:")), 2, QString::fromStdString(rootPath));

    //Change root directory for list of Folders with TEM data
    QString temp = accurateGPS;
    for(auto &entry : temDataDailyFolders){
        QString root = QString::fromStdString(entry.root_path().generic_string());
        fs::path temp = fs::path(entry.relative_path());
        entry = fs::path(rootPath);
        entry += temp;
    }

    //Data
    pathToLoopFolder = fs::path(rootPath+pathToLoopFolder.relative_path().generic_string());
    dataFolder = rootPath+fs::path(dataFolder).relative_path().generic_string();
    auto tempp = pathToLoopFolder.generic_string();
    tempp = dataFolder;
    tempp = pathToLoopFolder.generic_string();
}

void MainWindow::getVariablesFromIniFile(){
    ///Run to assign folders and files.

//    //TODO: get user to select the inifile
//        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
//                                                        ".",
//                                                        tr("INI files (*.ini)"));
//    ptree iniFile=readIniFile(fileName.toStdString());
    ptree iniFile=readIniFile("E:\\INIFILE.ini");
    assignPaths(iniFile);
}

void errorChecksOnFilesList(vector< fs::path > &filesList_){
    if (filesList_.empty()){
        throw runtime_error("No STK files discovered in directory");
    }
    if(any_of(filesList_.begin(),filesList_.end(),[&](fs::path &a){return !(fs::is_regular_file(a));})){
        throw runtime_error("Problem with some of the STK files");
    }
    if(any_of(filesList_.begin(),filesList_.end(),[&](fs::path &a){return (string(".stk").compare(a.extension().string()));})){
        throw runtime_error("Wrong extension on one of the STK files ");
    }
}

void errorChecksOnFileNameSurvey(QString fileNameSurvey){
    bool bugs;
    //...Do checks...
    if(bugs==true){
        throw runtime_error("problem opening "+fileNameSurvey.toStdString());
    }
}

void errorChecksOnFileNameLoop(fs::path a){
    //...Do checks...
    if(!fs::exists(a)) {
        throw runtime_error("Can't find loop file: "+a.generic_string());
        debugList<<"Can't find loop file: "+a.generic_string();
    }
}

std::vector<SVec> openLoopFile(fs::path pathToLoopFile_)
{
    ///Load UTM readings for loop shape

    //Coordinates from (.tx) loop file
    BPTokenizer loop(pathToLoopFile_);
    loop.Tokenize();
    vector<SVec> coordinates;
    for(size_t i=1; i<loop.nlines(); i++){
        double x,y,z;
        loop.getDouble(i,0,x);
        loop.getDouble(i,1,y);
        loop.getDouble(i,2,z);
        //error check:
        if(x!=x || y!=y || z!=z){
            throw runtime_error("Nan detected in loop file");
        }
        coordinates.push_back(SVec(x,y,z));
    }
    return coordinates;
}

component determineOrientation(std::string name){
    ///Determine if sensor is X,Y or Z

    size_t found = name.find("_X_");
    if (found != string::npos) return X;
    found = name.find("_Y_");
    if (found != string::npos) return Y;
    found = name.find("_Z_");
    if (found != string::npos) return Z;
}

vector<double> averageTogetherSeriesInTheseSTKFiles(vector<fs::path> stkFiles){
    ///Return average (stack of stacked waveforms)

    vector<double> avg;
    size_t N=stkFiles.size();
    for(auto x = stkFiles.begin();x!=stkFiles.end();++x){
        SM24_stk_reader reader;
        reader.initialize(*x);
        vector<double> data = reader.getStackedSeries();
        if(avg.empty()){
            avg=data;
        }
        else{
            if(avg.size() != data.size()){
                throw runtime_error("Mismatched sizes in averageTogetherSeriesInTheseSTKFiles: "+x->generic_string());
            }
            transform(data.begin(),data.end(),avg.begin(),avg.begin(),plus<double>());
        }
    }
    for_each(avg.begin(),avg.end(),[&N](double &x){ x=x/((double)N);});

    return avg;
}

bool sameComponentLoopAndLocation(stnProducts LandF,stnProducts x){
    ///For combining data from stations at same location
    //Orientation of probe
    string orientation1;
    size_t found1 = LandF.p.stem().generic_string().find("_X_");
    if(found1 != string::npos) orientation1="_X_";
    found1 = LandF.p.stem().generic_string().find("_Y_");
    if(found1 != string::npos) orientation1="_Y_";
    found1 = LandF.p.stem().generic_string().find("_Z_");
    if(found1 != string::npos) orientation1="_Z_";

    string orientation2;
    size_t found2 = x.p.stem().generic_string().find("_X_");
    if(found2 != string::npos) orientation2="_X_";
    found2 = x.p.stem().generic_string().find("_Y_");
    if(found2 != string::npos) orientation2="_Y_";
    found2 = x.p.stem().generic_string().find("_Z_");
    if(found2 != string::npos) orientation2="_Z_";

    bool UTMEisTheSame = x.location.first==LandF.location.first;
    bool UTMNisTheSame = x.location.second==LandF.location.second;
    bool LoopNumisTheSame = x.loopNum==LandF.loopNum;
    bool componentsAreTheSame = !orientation1.compare(orientation2);
    return UTMEisTheSame&&UTMNisTheSame&&componentsAreTheSame&&LoopNumisTheSame;
}

HFieldOfLoop getLoopGeometryFromFileAndCreateModel(loopNumber a){
    ///Extract geometry data from "fileNameLoop" and create model for calculating B-fields.
    //    fs::path pathToLoopFolder = "D:/TEM Data/LoopFiles/";
    fs::path pathToLoopFile;
    pathToLoopFile=pathToLoopFolder;
    switch (a) {
    case Loop_1:
        pathToLoopFile+=fs::path("Loop_1.tx");
        break;
    case Loop_2:
        pathToLoopFile+=fs::path("Loop_2.tx");
        break;
    case Loop_3:
        pathToLoopFile+=fs::path("Loop_3.tx");
        break;
    default:
        debugList<<"Problem selecting loop"<<endl;
        break;
    }
    errorChecksOnFileNameLoop(pathToLoopFile);
    std::vector<SVec> PolyLine = openLoopFile(pathToLoopFile);
    HFieldOfLoop loop(PolyLine,true);
    return loop;
}

map<string, vector<double> >  getPairsOfTimeAndLocationValuesFromGPS(){
    ///Accurate location data are stored in GPS folder. Relate the time of day of the measurement (found in the name of the stk file)
    /// to accurate location, using the crude GPS on the coil system as a reference.

    //Method #1: Associate Station File Names with GPS coordinates from GPS folder using the line and stn titles. Worked for Dec09 data
    //    if(false){
    //        //Get Station names as listed in lines of GPS file
    //        BPTokenizer tem(pathToTEM_File);
    //        tem.Tokenize();
    //        map<string,string> TimeAndName;
    //        for(size_t i=7; i<tem.nlines(); i++){
    //            string time;
    //            string northing,easting;
    //            tem.getString(i,2,easting);
    //            tem.getString(i,3,northing);
    //            tem.getString(i,19,time);
    //            //cut off decimals
    //            easting=easting.substr(0,easting.size()-3);
    //            northing=northing.substr(0,northing.size()-3);
    //            string name =easting+"E"+northing+"N";
    //            if(!time.empty()){
    //                TimeAndName[time]=name;
    //            }
    //        }

    //        //get UTM from GPS file
    //        BPTokenizer gps(pathToGPS_File);
    //        gps.Tokenize();
    //        map<string, vector<double> > NameAndGPSLocation;
    //        for(size_t i=1;i<tem.nlines();i++){
    //            string line,station;
    //            double northing,easting,elevation;
    //            gps.getString(i,0,line);
    //            gps.getString(i,1,station);
    //            string name =line+"E"+station+"N";
    //            //        if(i==147){
    //            //            bool here=1;
    //            //        }
    //            //        if(name=="2650E5300N"){
    //            //            bool sfsd=1;
    //            //        }
    //            gps.getDouble(i,2,easting);
    //            gps.getDouble(i,3,northing);
    //            gps.getDouble(i,6,elevation);
    //            NameAndGPSLocation[name]={easting,northing,elevation};
    //        }

    //        //Associate time to location
    //        map<string, vector<double> > TimeAndGPSLocation;
    //        for (const auto& [time, name] : TimeAndName) {
    //            //std::cout << time << " has value " << name << std::endl;
    //            string tempkey = time;
    //            string tempval = name;
    //            if(NameAndGPSLocation.find(name) != NameAndGPSLocation.end()){
    //                vector<double> locations=NameAndGPSLocation[name];
    //                TimeAndGPSLocation[time]=locations;
    //            }
    //        }
    //        return  TimeAndGPSLocation;
    //    }
    //    else{

    //Method #2 Make association by finding the closest match between crude UTM Northing/Easting of dat/tem File and accurate values in GPS folder
    BPTokenizer tem(pathToTEM_File);
    tem.Tokenize();
    vector<pair< pair<double,double>,string > > crudeUTM_STKfilename;
    map< string,string > name_loop;
    for(size_t i=7; i<tem.nlines(); i++){
        string time;
        string loopName;
        tem.getString(i,19,time);
        string northing,easting;
        tem.getString(i,5,easting);
        tem.getString(i,6,northing);
        tem.getString(i,14,loopName);
        char *stopstring,*stopstring2;
        double e,n;
        e = strtod(easting.c_str(), &stopstring);
        n = strtod(northing.c_str(), &stopstring2);
        if(strlen(stopstring)==0 && strlen(stopstring2)==0){
            if(n!=0.0 && e!=0.0){
                crudeUTM_STKfilename.push_back({{n,e},time});
                name_loop[time]=loopName;
            }
        }
    }
    //get accurate UTM from GPS file
    BPTokenizer gps(pathToGPS_File);
    gps.Tokenize();
    vector<pair<pair<double,double>, double> > accurateUTM;
    for(size_t i=1;i<gps.nlines();i++){
        string line,station;
        double northing,easting,elevation;
        gps.getString(i,0,line);
        gps.getString(i,1,station);
        string name =line+"E"+station+"N";
        gps.getDouble(i,2,easting);
        gps.getDouble(i,3,northing);
        gps.getDouble(i,6,elevation);
        //            if(){
        accurateUTM.push_back({{northing,easting},elevation});
        //            }
    }

    //Associate time to location
    size_t ptsRejected=0;
    map<string, vector<double> > TimeGPSLocationAndLoop;
    for_each(crudeUTM_STKfilename.begin(),crudeUTM_STKfilename.end(),
             [&accurateUTM,&TimeGPSLocationAndLoop,&name_loop,&ptsRejected](pair<pair<double,double>, string> &cru){
        //Find matching accurate reading
        pair<double,double> cr=cru.first;
        auto it = min_element(accurateUTM.begin(),accurateUTM.end(),
                              [&cr,&ptsRejected](auto a,auto b){
            auto x1 = a.first;
            auto x2 = b.first;
            double d1 = sqrtl((x1.first-cr.first)*(x1.first-cr.first)+(x1.second-cr.second)*(x1.second-cr.second));
            double d2 = sqrtl((x2.first-cr.first)*(x2.first-cr.first)+(x2.second-cr.second)*(x2.second-cr.second));
            return(d1<d2); //distance
        });
        //make sure within 20 m of accurate reading or discard point
        pair<double,double> closestAcc=it->first;
        double distance =sqrtl((closestAcc.first-cr.first)*(closestAcc.first-cr.first)+(closestAcc.second-cr.second)*(closestAcc.second-cr.second));
        if(distance<20){
            cru.first=closestAcc;
            //Need to transfer to map
            string time = cru.second;
            double loopNumber=99999;
            string temp = name_loop[time];
            if(!name_loop[time].compare("Loop_1")){
                loopNumber=1;
            }
            if(!name_loop[time].compare("Loop_2")){
                loopNumber=2;
            }
            if(!name_loop[time].compare("Loop_3")){
                loopNumber=3;
            }
            TimeGPSLocationAndLoop[time]={it->first.second,it->first.first,it->second,loopNumber};
        }
        else{
            ptsRejected++;
        }
    });
    debugList<<" ptRejected_distance "<<ptsRejected<<" ";
    return  TimeGPSLocationAndLoop;
}

vector<vectorCompStackedWaveform> loadStationData(loopNumber loop, component c, const size_t N, fs::path spf){
    ///Bring data from user specified specialProcessingFolder "spf" into structure

    if(!fs::exists(spf)){
        debugList<<"problem finding "<<spf<<endl;
        throw runtime_error(spf.generic_string()+" doesn't exist");
    }

    //Information on binaries is stored in csv file.
    fs::path infoFile = spf;
    infoFile/="info.csv";
    if(!fs::exists(infoFile)){
        throw runtime_error(infoFile.generic_string()+" doesn't exist");
    }
    BPTokenizer info(infoFile);
    info.Tokenize();
    vector<vectorCompStackedWaveform> waveForms;
    for(size_t i=1;i<info.nlines();i++){

        //Name of binary file
        string name;
        info.getString(i,0,name);
        fs::path it=spf;
        it/=fs::path(name);
        it+=fs::path(".bin");

        if(fs::is_regular_file(it)){

            //Proceed if Orientation of probe in data in file matches:
            component cf = determineOrientation(name);
            if (cf==c){
                SVec orientation;
                switch (cf) {
                case X:
                    orientation=SVec(1,0,0);
                    break;
                case Y:
                    orientation=SVec(0,1,0);
                    break;
                case Z:
                    orientation=SVec(0,0,1);
                    break;
                default:
                    string message="Problem deteriming component: ";
                    message+=cf;
                    throw runtime_error(message);
                    break;
                }

                //Proceed if loop number matches:
                int loopNumInCSV;
                info.getInt(i,6,loopNumInCSV);
                if(loop==loopNumInCSV){

                    //Proceed if sample number matches:
                    int N_;
                    info.getInt(i,5,N_);
                    if(N_<=0) throw runtime_error("Invalid sampleNumber "+to_string(N_));
                    if(N_==N){
                        //Station Location
                        double x,y,z;
                        info.getDouble(i,1,x);
                        info.getDouble(i,2,y);
                        info.getDouble(i,3,z);
                        SVec coordinates(x,y,z);

                        debug<<it.generic_string()<<endl;

                        //Get binary data
                        ifstream ifile(it.generic_string(), ios::binary);
                        ifile.seekg(0, ios::end);
                        size_t fileSize = ifile.tellg();
                        if(fileSize%sizeof(double) != 0){
                            throw runtime_error(it.generic_string()+" is not correct size for doubles: "+to_string(fileSize)+" bytes");
                        }
                        vector<double> bytes;
                        bytes.resize(fileSize/sizeof(double), 0);
                        ifile.seekg(0, ios::beg);
                        ifile.read((char*)&bytes[0], fileSize);
                        ifile.close();

                        //                        todoList<<it.parent_path().parent_path().append("stacked").append(it.stem().generic_string()).concat(".stk").generic_string()<<endl;
                        //                        todoList<<it.generic_string()<<endl

                        vectorCompStackedWaveform local(name,coordinates);
                        local.initialize(bytes,orientation);
                        waveForms.push_back(local);
                    }
                }
            }
        }
    }
    return waveForms;
}

void extractInfoAndDataFromSTK_FilesThenMakeTableAndBinaries(fs::path folder,const  map<string, vector<double> > timeLocationLoopInfo){
    ///Extract data from folder containing STK files, combine with timeLocationInfo data (from GPS) to produce csv table
    /// describing each of the raw stk files. "infoOnRawData.csv"
    ///Save the combined data of repeat measurement stk files as "bin" files in special processing folder. These are the stacks of stacked data (averaged multiple readings)
    /// Produce a second csv with descriptions of the bin files. "info.csv"

    errorChecksOnFilesList(listSTKfiles);

    //Make a vector of coordinates, and filename pairs
    size_t failedToIdentify =0;
    vector<stnProducts> stations;
    for_each(listSTKfiles.begin(),listSTKfiles.end(),[&timeLocationLoopInfo, &stations,&failedToIdentify](fs::path &x)
    {
        //Extract two XML files within STK and access needed info
        SM24_stk_reader reader;
        string tempName = x.generic_string();
        reader.initialize(x);
        string time = reader.getTime(); //Produces (human) readable xml files in the stk location.
        reader.deleteDI(); //clean up unneeded xml file
        if(timeLocationLoopInfo.find(time) != timeLocationLoopInfo.end()){
            vector<double> w = timeLocationLoopInfo.at(time);
            // stnProducts(std::pair<double,double> location, double elevation, std::filesystem::path pathToSTK,double loopNumber)
            stations.push_back(stnProducts({w[0],w[1]},w[2],x,w[3]));
        }
        else{
            //            todoList<<endl<<time<<endl;
            failedToIdentify++;
        }
    });
    debugList<<" failedToIdentify: "<<failedToIdentify<<" of "<<listSTKfiles.size()<<" ";

    size_t counter=0;

    //Set up columns for tables:
    ofstream csv(folder.generic_string()+"/info.csv");
    ofstream csv_raw(folder.generic_string()+"/infoOnRawData.csv");
    csv<<"File,UTME,UTMN,Elev,Component,N,Loop"<<endl;
    csv_raw<<"File,UTME,UTMN,Elev,RepeatedMeasurementGroup,Loop"<<endl;

    //Find readings with identical location and component, average them together and write to binaries in folder
    while(!stations.empty()){
        //Keep same time based name for each component of the station
        sort(stations.begin(),stations.end(),[](stnProducts &x,stnProducts &y) -> bool
        {
            return x.p.generic_string()<y.p.generic_string();
        });

        //Find repeat measurements and average
        const stnProducts stn1=stations.front();
        vector<stnProducts> multipleReadings, others;
        size_t n = count_if (stations.begin(), stations.end(),
                             [&stn1](auto x){
            return sameComponentLoopAndLocation(stn1,x);
        });
        multipleReadings.resize(n);
        others.resize(stations.size()-n);
        partition_copy(stations.begin(), stations.end(), multipleReadings.begin(),others.begin(),
                       [&stn1](auto x){
            return sameComponentLoopAndLocation(stn1,x);}
        );
        vector<fs::path> stkFiles;
        for_each(multipleReadings.begin(),multipleReadings.end(),[&stkFiles](stnProducts x){stkFiles.push_back(x.p);});
        vector<double> avg = averageTogetherSeriesInTheseSTKFiles(stkFiles);

        //Record Which files correspond to individual location
        static size_t repeatMeasurementGroup = 1;
        for_each(multipleReadings.begin(),multipleReadings.end(),[&csv_raw](stnProducts x)
        {
            //        vector< pair< pair<double,double>, fs::path > > multipleReadings, others;
            csv_raw<<std::fixed << std::setprecision(2);
            csv_raw<<x.p.stem()<<",";
            csv_raw<<x.location.first<<",";
            csv_raw<<x.location.second<<",";
            csv_raw<<x.elevation<<",";
            csv_raw<<std::fixed << std::setprecision(0);
            csv_raw<<repeatMeasurementGroup<<",";
            csv_raw<<x.loopNum<<endl;
        });
        repeatMeasurementGroup++;

        //Write to folder
        csv<<std::fixed << std::setprecision(2);
        csv<<stn1.p.stem().generic_string()<<",";
        csv<<stn1.location.first<<","<<stn1.location.second<<",";
        csv<<stn1.elevation<<",";
        //Get component:
        csv<<repeatMeasurementGroup<<",";
        csv<<avg.size()<<",";
        csv<<stn1.loopNum<<endl;
        //Binary data:
        string dataFileName = folder.generic_string()+"/"+stn1.p.stem().generic_string()+".bin";
        ofstream dfile(dataFileName,ios_base::binary);
        dfile.write((char*)&avg[0],sizeof(double)*avg.size());
        dfile.close();

        counter++;

        //Resize
        stations=others;
    }
    debugList<<" "<<counter<<" files produced ";
    csv.close();
    csv_raw.close();
}

void collectAndReorganizeStationInfofromFolders(fs::path specialProcessingFolder){
    ///Extract station locations from precision GPS (in GPS folder). Use these to refine the (crude) survey location recorded for
    /// each stk. Create a table summarizing these needed parts ready for processing.
    /// Stack the stk files of identical coil orientation, loop number and time making binary "bin" files.

    fs::create_directory(specialProcessingFolder); //Put beside day's data

    map<string, vector<double> > timeLocationAndLoopInfo = getPairsOfTimeAndLocationValuesFromGPS(); //Precision GPS is separate instrument from EM system.
    if(timeLocationAndLoopInfo.empty()){
        debugList<<"TimeLocationInfo empty";
        throw runtime_error("TimeLocationInfo empty");
    }
    extractInfoAndDataFromSTK_FilesThenMakeTableAndBinaries(specialProcessingFolder, timeLocationAndLoopInfo);
}

void plotResult(std::vector< stnProducts > result, optional<fs::path> csvLocation = nullopt){
    ///Maybe later using Qt graphing

    if(csvLocation){

        //Just write to text file
        fs::path folder = csvLocation.value();
        if(!fs::exists(folder.parent_path())){
            throw runtime_error("Can't find: "+folder.parent_path().generic_string());
        }
        ofstream csv(folder.generic_string());

        //Head
        csv<<"UTME,UTMN,Elevation,";
        for(size_t i=0;i<result[0].MTWFRs.size();i++){
            //            char temp=65+i;
            //            char M=65+12;
            //            char T=65+15;
            //            char R=65+17;
            //            csv<<temp<<",";
            csv<<"Measurement,Theory,Weight,Fit,Residual,";
        }
        csv<<endl;

        //Numbers
        for_each(result.begin(),result.end(),[&csv](stnProducts i){
            csv<<std::fixed << std::setprecision(2);
            csv<<to_string(i.location.first)<<",";
            csv<<to_string(i.location.second)<<",";
            csv<<to_string(i.elevation)<<",";
            for_each(i.MTWFRs.begin(),i.MTWFRs.end(),[&csv](stnProducts::MTWFR j){
                csv<<std::fixed << std::scientific;
                csv<<j.measurement<<",";
                csv<<j.theoretical<<",";
                csv<<j.weight<<",";
                csv<<j.fit<<",";
                csv<<j.residual;
            });
            csv<<endl;
        });
        csv<<endl;
    }
}

void MainWindow::on_pushButton_Go_clicked()
{
    ///Measure amplitude of last quarter of waveform (or whatever section specified by user).
    /// Calculate theoretical field vector for every station of survey and least-squares fit the measurements to
    /// theory.
    /// Report results and residual values to a csv for gridding.
    /// Do this for every loop and component (X,Y,Z).
    try {
        //Debug logs
        debug = ofstream(fs::current_path().generic_string()+"/completed.txt");
        debugList = ofstream(fs::current_path().generic_string()+"/debug.txt");
        getVariablesFromIniFile();

        //Load data from specialProcessing folder.
        for(loopNumber loop : {Loop_1,Loop_2,Loop_3}){ //
            const size_t N = 240000;
            HFieldOfLoop loopSimulation = getLoopGeometryFromFileAndCreateModel(loop);

            vector<pair<size_t,size_t> > timeGates;
            timeGates.push_back(pair<size_t,size_t>(180000,239999)); //Last quarter of stacked series.

            //Residuals for each component
            for(component axis : {Z}){ //{X,Y,Z}){ //
                vector<vectorCompStackedWaveform> rawStnData;
                vector<fs::path> foldersOfSTKfiles = temDataDailyFolders;
                for_each(foldersOfSTKfiles.begin(),foldersOfSTKfiles.end(),[&rawStnData,&loop, &axis, &N](fs::path p){
                    vector<vectorCompStackedWaveform> rsd= loadStationData(loop, axis, N, p.append("specialProcessing"));
                    rawStnData.insert(rawStnData.end(),rsd.begin(),rsd.end());
                });

                if(rawStnData.empty()) throw runtime_error("didn't read any data");

                OnTimeSignalSurvey processingJob;
                processingJob.initializeOTSS(rawStnData,loopSimulation,N);
                optional<vector<stnProducts> > stnResults = processingJob.specialProcessing(timeGates);

                //        processingJob.process(300); //Some number of FIR taps
                //        //Station locations and timeGateValues:
                //        auto stnResults = processingJob.getWindowedData(timeGates);

                //-------------------------------------------------
                //"stnResults" are the final product.
                //-------------------------------------------------

                if(stnResults){
                    plotResult(*stnResults,dataFolder+to_string(component(axis))+"Loop"+to_string(loop)+"residuals.csv");
                }
                else {
                    throw runtime_error("Problem getting windowed Data");
                }
            }
        }
    }  catch (const std::exception& ex) {
        debugList << "*** Error: " << ex.what() << "\n";
        debugList.close();
    }
}

void MainWindow::on_pushButton_Reorganize_clicked()
{
    ///Gather together data series ready for processing and place in a "specialProcessing" folder,
    /// one for each day's readings.
    /// Average together repeat readings.
    try {
        debugList<<"Reorganize_clicked ";
        getVariablesFromIniFile();

        //Process each folder in list. Report whether processing is complete
        vector<fs::path> rawFiles = temDataDailyFolders;
        for_each(rawFiles.begin(),rawFiles.end(),
                 [&](fs::path x){
            //Each folder has STK files containing coil data
            listSTKfiles.clear();
            QString dir = QString::fromStdString(x.generic_string());
            findDatFile(dir);
            debugList<<"findDatFile ";
            findStackedFolder(dir);
            debugList<<"findStackedFolder ";
            dir.append("/specialProcessing");
            pathToGPS_File = fs::path(accurateGPS.toStdString());

            //Result of this process will be stored in a specialProcessingFolder
            fs::path specialProcessingFolder= fs::path(dir.toStdString());//
            debugList<<dir.toStdString()<<endl;
            collectAndReorganizeStationInfofromFolders(specialProcessingFolder);
        });

    }  catch (runtime_error &a) {
        debugList<<a.what();
    }
    debugList.close();
}

void MainWindow::findDatFile(QString dir)
{
    ///get TEM file

    //    QString temp="C:/Users/Public/Crystal_Lake_2022/Dec09/Dec09/Dec09.tem";
    QString name = QString::fromStdString(fs::path(dir.toStdString()).stem().generic_string());
    QString temp=dir+"/"+name+".dat";
    pathToTEM_File = fs::path(temp.toStdString());

    if(!exists(pathToTEM_File)){
        throw runtime_error(temp.toStdString()+" doesn't exist");
    }
    //    ui->TEM_File_label->setText(QString::fromStdString("TEM File loaded: "+pathToTEM_File.stem().generic_string()));
}

void MainWindow::findStackedFolder(QString dir){
    ///Locate survey data from user selected folder. Make list of stk files ready for processing

    using namespace std;
    //QString dir("C:\\Users\\Public\\Crystal_Lake_2022\\Dec01\\stacked\\");
    dir.append("/stacked/");

    //Create list of folders and a list of files
    for(fs::directory_iterator it = fs::directory_iterator(dir.toStdString()); it != fs::directory_iterator(); ++it)
    {
        if(it->is_regular_file()){
            if(!(it->path().extension().string().compare(".stk"))){
                listSTKfiles.push_back(it->path());
            }
        }
    }

    //    listSTKfiles.resize(10);//Debug

    //Update display
    //    ui->filesLoaded_label->setText("STK Files Located: "+QString::number(listSTKfiles.size()));
}
