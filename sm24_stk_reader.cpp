#include "sm24_stk_reader.h"
#include "pugixml.hpp"
#include "pugiconfig.hpp"

#include <exception>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
using namespace pugi;
namespace fs = std::filesystem;

bool isEqualToCRLF (char i, char j) {
    bool cr = (i=='\r');
    bool lf = (j=='\n');
    return (cr&&lf);
}

std::vector<size_t> indices_of_1st_5_CRLF(vector<char> bytes){
    ///Find index of end of first 5 CR/LF in values in file. These mark the beginning and
    ///end points surrounding the two XML files and the start of the binary data

    vector<size_t> indices;
    vector<char>::iterator it = bytes.begin();
    for(size_t i=0;i<5;i++){
        size_t index;
        it = std::adjacent_find (++it, bytes.end()-1, isEqualToCRLF);
        if (it==(bytes.end()-1)){
            throw runtime_error("Less than 5 CR/LF in STK file");
        }
        index=it-bytes.begin();
        indices.push_back(index);
    }
    //    isEqualToCRLF(*it,*(it+1));
    //    it++;
    //    it++;
    //    isEqualToCRLF(*it,*(it+1));
    return indices;
}

const std::vector<double> &SM24_stk_reader::getStackedSeries() const
{
    return stackedSeries;
}

xml_document SM24_stk_reader::loadXMLfileInPugi(fs::path sdi)
{
    pugi::xml_document doc;
    if (!doc.load_file(sdi.generic_wstring().c_str())){
        throw runtime_error("Can't load "+sdi.generic_string());
    }
    return doc;
}

string SM24_stk_reader::SDI_FileName(){
    ///Name of "...StackedDataInfo.xml" file produced for this SDK file
    string xmlName = datafilePath.parent_path().generic_string()+"/";
    xmlName+=datafilePath.stem().generic_string();
    xmlName+="StackDataInfo.xml";
    return  xmlName;
}

string SM24_stk_reader::DI_FileName(){
    ///Name of "...DataInfo.xml" file produced for this SDK file
    string xmlName = datafilePath.parent_path().generic_string()+"/";
    xmlName+=datafilePath.stem().generic_string();
    xmlName+="DataInfo.xml";
    return  xmlName;
}

void SM24_stk_reader::initialize(filesystem::path filepath)
{
    ///Make sure STK binary file exists, open and find location of carriage-returns "\r\n" in file.
    /// There should only be 5. Then read data into member variables.

    //Error Check
    if(!fs::exists(filepath)){
        throw runtime_error(filepath.generic_string()+" doesn't exist");
    }

    datafilePath=filepath;

    ifstream ifile(filepath.generic_string(), ios::binary);
    ifile.seekg(0, ios::end);
    size_t fileSize = ifile.tellg();
    cout << "file is " << fileSize << "bytes" << endl;
    vector<char> bytes;
    bytes.resize(fileSize, 0);
    ifile.seekg(0, ios::beg);
    ifile.read(&bytes[0], fileSize);
    cout << "read in " << ifile.gcount() << "bytes" << endl;

    CRFI = indices_of_1st_5_CRLF(bytes);
    auto tempChar = CRFI;
    if(CRFI[4]+2 >= bytes.size()){
        throw runtime_error("5th carriage return at end of file, no stacking data present");
    }

    cout<<"Carriage Returns detected at ";
    for(char c: CRFI){
        cout<<c<<",";
    }
    cout<<endl;

    //Load in data for two xml files
    dataInfo.assign(bytes.begin()+CRFI[0]+2,bytes.begin()+CRFI[1]);
    stackDataInfo.assign(bytes.begin()+CRFI[2]+2,bytes.begin()+CRFI[3]);

    //Run through bytes and convert each to double:
    if((bytes.size() - (CRFI[4]+2))%8 != 0){
        throw runtime_error("The file data type is wrong size for containing all doubles");
    }

    //Convert groups of 8 char units to double:
    vector<char> ch;
    ch.assign(8,'a');
    for(size_t i=CRFI[4]+2;i<bytes.size();i=i+8){
        ch[0]=bytes[i];
        ch[1]=bytes[i+1];
        ch[2]=bytes[i+2];
        ch[3]=bytes[i+3];
        ch[4]=bytes[i+4];
        ch[5]=bytes[i+5];
        ch[6]=bytes[i+6];
        ch[7]=bytes[i+7];
        double *p = reinterpret_cast<double*>(&ch[0]);
        if(string(typeid(*p).name()).compare("double")){
            throw runtime_error("index "+to_string(i/8)+" is not double");
        }
        stackedSeries.push_back(*p);
    }

    initialized=true;
}

bool SM24_stk_reader::writeDataInfoXMLToFile()
{
    ///Write "...DataInfo.xml" in datafolder specific to this sdk
    if(initialized){
        ofstream wf(DI_FileName(), ios::out | ios::binary);
        if(!wf) {
            throw runtime_error("Cannot open file "+DI_FileName());
        }
        wf.write((char *) &dataInfo[0], dataInfo.size()*sizeof(char));
        if(!wf.good()) {
            throw runtime_error("Cannot open file "+DI_FileName());
        }
        wf.close();
        return 1;
    }
    else{
        return 0;
    }
}

bool SM24_stk_reader::writeStackDataInfoXMLToFile()
{
    ///Write "StackDataInfo.xml" to datafolder
    if(initialized){
        ofstream wf(SDI_FileName(), ios::out | ios::binary);
        if(!wf) {
            throw runtime_error("Cannot open file "+ SDI_FileName());
        }
        wf.write((char *) &stackDataInfo[0], stackDataInfo.size()*sizeof(char));
        if(!wf.good()) {
            throw runtime_error("Cannot open file "+SDI_FileName());
        }
        wf.close();
        return 1;
    }
    else{
        return 0;
    }
}

string SM24_stk_reader::getName()
{
    ///Get name of station from corresponding "...DataInfo.xml" in dataFolder

    fs::path sdi(DI_FileName());
    //Write file if doesn't already exist
    if(!fs::exists(sdi)){
        writeDataInfoXMLToFile();
    }

    pugi::xml_document doc=loadXMLfileInPugi(DI_FileName());
    string name = doc.child("DataInfo").child("SysInfo").child_value("Name");
    return name;
}

string SM24_stk_reader::getTime()
{
    ///Get name of station from corresponding "...DataInfo.xml" in dataFolder

    fs::path sdi(DI_FileName());
    //Write file if doesn't already exist
    if(!fs::exists(sdi)){
        writeDataInfoXMLToFile();
    }

    pugi::xml_document doc=loadXMLfileInPugi(DI_FileName());
    string name = doc.child("DataInfo").child("SysInfo").child_value("DateTime");
    return name;
}

void SM24_stk_reader::deleteDI()
{
    ///"Delete Data Info" file
    fs::path sdi(DI_FileName());
    //Write file if doesn't already exist
    if(fs::exists(sdi)){
        fs::remove(sdi);
    }
}

//string SM24_stk_reader::getLocation()//fs::path pathToLoopFile)
//{
//    ///Get name of station from "StackDataInfo.xml" in dataFolder

//    ////First get loopId
//    //    pugi::xml_document doc=loadXMLfileInPugi(datafolder.generic_string()+"/StackDataInfo.xml");
//    //    string loopId = doc.child("StackDataInfo").child("TimingInfo").child_value("LoopId");

//    //    //Now get coordinates from loop file
//    return string();
//}
