#ifndef ONTIMESIGNALSURVEY_H
#define ONTIMESIGNALSURVEY_H

#include <vector>
#include <optional>
#include <filesystem>

#include "SVec.h"

class vectorCompStackedWaveform;
class HFieldOfLoop;
class SeriesType;

namespace OnTimeSignalSurveyTools {
struct stnProducts
{
    ///Lightweight holder of station info and products ready to be written to file or plotted.
public:
    stnProducts(std::pair<double,double> loc, double e, std::filesystem::path p_,double ln):location(loc),elevation(e),p(p_),loopNum(ln){}
    stnProducts(){}
    struct MTWFR{
        MTWFR(){}
        MTWFR(double m,double t,double w,double f, double r):measurement(m),theoretical(t),weight(w),fit(f),residual(r){}
        double measurement;
        double theoretical; //Field calculated using loop geometry
        double weight;  // (1/theoretical)
        double fit;      // theoretical * scaling factor from least squares.
        double residual; // (measurement - fit)/(fit) X 100%
     };

    std::pair<double,double> location;
    double elevation;
    std::filesystem::path p;
    double loopNum;
    std::vector<MTWFR> MTWFRs;
    std::vector<double> residuals;
};
}

class OnTimeSignalSurvey
{
    ///GOAL:
    ///Transform TEM survey data into a form which can be used to detect high-conductors using the on-time signal.

    ///PROCESS STEPS:
    ///
    ///Estimate primary field waveform using survey data. Do this by combining the waveforms of all the stations weighting each
    ///according to threoretical field strength S^4 and the dotproduct of the coil orientation to the theoretical field.
    ///
    ///Create a deconvolution FIR filter to transform the (measured) primary field to an ideal 4-cycle squarewave.
    ///
    ///Apply deconvolution filter to original field data
    ///
    /// Measure signal strength in sections of the filtered data to create a new map of signal decay potentially revealing
    /// high conductors missed in the original survey which only used the off-time signal.
    ///
public:
    bool initializeOTSS(const std::vector<vectorCompStackedWaveform> &stationData,
                        const HFieldOfLoop &loop,
                        const size_t N_);
    bool process(const size_t taps);

    std::optional< std::vector< OnTimeSignalSurveyTools::stnProducts > > specialProcessing(const std::vector<std::pair<size_t,size_t> > &timeGates);
    std::optional< std::vector< std::pair<SVec,std::vector<double> > > > getWindowedData(std::vector<vectorCompStackedWaveform> stationData, const std::vector<std::pair<size_t,size_t> > &timeGates, SeriesType type);

private:
    std::vector<double> weightedStacking();
    std::vector<double> calculateFilter(const size_t taps,const std::vector<double> mPrimary);
    void applyToOriginalData(const std::vector<double> firValues);
    //
    std::unique_ptr<HFieldOfLoop> mLoop;
    std::vector<vectorCompStackedWaveform> mStationData;
    size_t N=32000; //default value
};

#endif // ONTIMESIGNALSURVEY_H
