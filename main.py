# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['font.size'] = 14

def read_work_bench_xyz_mod(fname):
    with open(fname, 'r') as fid:
        df = pd.read_csv(fname)

    easting = np.asarray(df.UTME.values)
    northing = np.asarray(df.UTMN.values)
    elev = df.Elevation.values
    residual = np.asarray(df.Residual.values)
    utm_coords = np.column_stack((easting, northing))
    utm_data = data_utm = np.column_stack((easting,northing,residual))

    # if conversion from utm to lat lon is required:
    import utm
    lat=np.zeros((len(easting),))
    lon=np.zeros((len(northing),))
    i=0
    for x,y in utm_coords:
        X,Y = utm.to_latlon(x, y, 17, 'N')
        lat[i] = X
        lon[i] = Y
        i+=1

    return df, easting, northing, utm_coords, utm_data, residual

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # print_hi('PyCharm')
    import pygmt

    ColorBar = pygmt.makecpt(cmap="jet", series="-10/10/1",continuous=True, reverse=False,no_bg=False) #Loop 3

    file_name = "E:/TEM Data/ZLoop1residuals.csv"
    loopDataFile = "E:/TEM Data/LoopFiles/Loop_1.tx"
    LoopFrame = ["a", '+t"Loop 1 Residual"']
    # ColorBar = pygmt.makecpt(cmap="jet", series="2/10/1",continuous=True, reverse=False,no_bg=False) #Loop 1

    # file_name = "E:/TEM Data/ZLoop2residuals.csv"
    # LoopFrame = ["a", '+t"Loop 2 Residual"']
    # ColorBar = pygmt.makecpt(cmap="jet", series="-5/1/1",continuous=True, reverse=False,no_bg=False) #Loop 2
    # loopDataFile = "E:/TEM Data/LoopFiles/Loop_2.tx"

    # file_name = "E:/TEM Data/ZLoop3residuals.csv"
    # LoopFrame = ["a", '+t"Loop 3 Residual"']
    # ColorBar = pygmt.makecpt(cmap="jet", series="-3/3/1",continuous=True, reverse=False,no_bg=False) #Loop 3
    # loopDataFile = "E:/TEM Data/LoopFiles/Loop_3.tx"

    df, easting, northing, utm_coords, utm_data, residual = read_work_bench_xyz_mod(file_name)

    ## Step 2: PyGmt Implementation

    ## Gridding & Interpolation (Blockmean, Blockmedian, Surface)


    # define region with max - min extent of coords
    utme_min, utme_max = np.amin(easting), np.amax(easting)
    utmn_min, utmn_max = np.amin(northing), np.amax(northing)
    utm_region =[utme_min, utme_max, utmn_min, utmn_max]
    if (utmn_max - utmn_min) > (utme_max - utme_min): #Equal sides, a square
        space = ((utmn_max - utmn_min)-(utme_max - utme_min)) / 2
        utm_region[0] -= space
        utm_region[1] += space
    else:
        space = ((utme_max - utme_min)-(utmn_max - utmn_min)) / 2
        utm_region[2] -= space
        utm_region[3] += space

    Bmean = pygmt.blockmean(
        data=utm_data, region=utm_region, spacing=["100e"])

    # Bmean_new = pygmt.blockmedian(
    #     data=utm_data, region=utm_region, spacing=["60e"])

    MeanSurface =pygmt.surface(Bmean, spacing = ['20e'])

    # MeanSurface_new =pygmt.surface(Bmean, spacing = ['60e'])

    ## Plotting 2D gridded data in PyGmt (grdimage, makecpt)

    fig = pygmt.Figure()

    # # loopDataFile = "E:/TEM Data/LoopFiles/Loop_1.tx"
    # # LoopFrame = ["a", '+t"Loop 1 Residual"']
    # # ColorBar = pygmt.makecpt(cmap="jet", series="2/10/1",continuous=True, reverse=False,no_bg=False) #Loop 1
    #
    # LoopFrame = ["a", '+t"Loop 2 Residual"']
    # ColorBar = pygmt.makecpt(cmap="jet", series="-5/1/1",continuous=True, reverse=False,no_bg=False) #Loop 2
    # loopDataFile = "E:/TEM Data/LoopFiles/Loop_2.tx"
    #
    # # LoopFrame = ["a", '+t"Loop 3 Residual"']
    # # ColorBar = pygmt.makecpt(cmap="jet", series="-3/3/1",continuous=True, reverse=False,no_bg=False) #Loop 3
    # # loopDataFile = "E:/TEM Data/LoopFiles/Loop_3.tx"

    # ColorBar = pygmt.makecpt(cmap="jet", series="-10/19/1",continuous=True, reverse=False,no_bg=False) # Wide Range

    df_loop = pd.read_csv(loopDataFile,
                          sep=' ',
                          header=None,
                          usecols=[0, 1],
                          names=['UTMEL', 'UTMNL']
                          )
    df_loop = df_loop[1:df_loop.size]
    # fig.show()
    # create colorbar

    #create gridimage to be plotted
    fig.grdimage(
        grid=MeanSurface,
        region=utm_region,
        frame=LoopFrame,
        cmap=ColorBar
    )

    # add colorbar to image
    fig.colorbar(frame=["a", "x+lResidual"])
    frame=["WSne", "xaf+lx-axis", "yaf+ly-axis"],

    #Add dots representing sample points
    fig.plot(x=easting, y=northing,  style="c0.2c", cmap=True, color=utm_data.flatten()[2::3], pen="black")

    # #Include loop shape in image:
    fig.plot(x=np.float32(df_loop.UTMEL), y=np.float32(df_loop.UTMNL),  style="c0.1c", color="black", pen="black")
    fig.show()

    # Creating subplots & experimenting with grid spacing

    # fig = pygmt.Figure()

    # with fig.subplot(
    #     nrows=1, ncols=2, figsize=("13.5c", "4c"), title="interpolation spacing"
    # ):
    #     # Plot 1
    #
    #     with fig.set_panel(panel=0):
    #         ColorBar = pygmt.makecpt(cmap="jet", series="-10/19/1", continuous=True, reverse=False, no_bg=False)
    #
    #         fig.grdimage(grid=MeanSurface, region=utm_region, frame=["a"], cmap=ColorBar)
    #         fig.colorbar(frame=["WSne", "x+lResidual"])
    #     # Plot a histogram showing the z-value distribution in the original digital
    #     # elevation model
    #     with fig.set_panel(panel=1):
    #         ColorBar = pygmt.makecpt(cmap="jet", series="-10/19/1",continuous=True, reverse=False,no_bg=False)
    #         # fig.grdimage(grid=MeanSurface_new,region=utm_region,frame=["wnSE"],cmap=ColorBar)
    #         fig.colorbar(frame=["a", "x+lResidual"])
    #
    # fig.show()
    #
    # ## Color bar parameters in PyGmt
    #
    # fig = pygmt.Figure()
    #
    # with fig.subplot(
    #     nrows=1, ncols=3, figsize=("13.5c", "4c"), title="interpolation spacing"
    # ):
    #     # Plot 1
    #
    #     with fig.set_panel(panel=0):
    #         ColorBar1 = pygmt.makecpt(cmap="jet", series="-10/20/3",continuous=True,
    #     reverse=False,no_bg=False)
    #
    #         ColorBar2 = pygmt.makecpt(cmap="jet", series="-5/10/0.5",continuous=True,
    #     reverse=False,no_bg=False)
    #
    #         ColorBar3 = pygmt.makecpt(cmap="viridis", series="-5/10/0.5",continuous=False,
    #     reverse=False,no_bg=False)
    #
    #         fig.grdimage(grid=MeanSurface,region=utm_region,frame=["a"],cmap=ColorBar2)
    #         fig.colorbar(frame=["WSne", "x+lResidual"])
    #     # Plot a histogram showing the z-value distribution in the original digital
    #     # elevation model
    #     with fig.set_panel(panel=1):
    #
    #         ColorBar = pygmt.makecpt(cmap="jet", series="-10/19/1",continuous=True,
    #     reverse=False,no_bg=False)
    #
    #         fig.grdimage(grid=MeanSurface,region=utm_region,frame=["wnSE"],cmap=ColorBar1)
    #
    #         fig.colorbar(frame=["WSne", "x+lResidual"])
    #
    #     with fig.set_panel(panel=2):
    #
    #         fig.grdimage(grid=MeanSurface,region=utm_region,frame=["wnSE"],cmap=ColorBar3)
    #
    #         fig.colorbar(frame=["WSne", "x+lResidual"])
    #
    # fig.show()
    #
    #
    #
    # ## Step 3 Scipy & Matplotlib implementation
    #
    # ## Gridding & Interpolation (griddata, imshow)
    #
    # grid_x, grid_y = np.mgrid[utme_min:utme_max:1000j, utmn_min:utmn_max:1000j]
    #
    # from scipy.interpolate import griddata
    #
    #
    # LinearGrid = griddata(utm_coords, residual, (grid_x, grid_y), method='linear')
    # CubicGrid = griddata(utm_coords, residual, (grid_x, grid_y), method='cubic')
    #
    # plt.figure(figsize=(10, 3.5))
    #
    # plt.subplot(1, 2, 1)
    # plt.imshow(LinearGrid.T,extent=(utme_min,utme_max,utmn_min,utmn_max), origin='lower',cmap='jet')
    # plt.colorbar(extend='both')
    #
    # plt.subplot(1, 2, 2)
    # plt.imshow(CubicGrid.T,extent=(utme_min,utme_max,utmn_min,utmn_max), origin='lower',cmap='jet')
    # plt.colorbar(extend='both')
    #
    # plt.show()
    #
    #
    # ## Colorbar options in matplotlib (extend, clim, normalize)
    #
    # from matplotlib.colors import Normalize, LogNorm, SymLogNorm
    # grid_x, grid_y = np.mgrid[utme_min:utme_max:1000j, utmn_min:utmn_max:1000j]
    #
    # from scipy.interpolate import griddata
    #
    #
    # LinearGrid = griddata(utm_coords, residual, (grid_x, grid_y), method='linear')
    # CubicGrid = griddata(utm_coords, residual, (grid_x, grid_y), method='cubic')
    #
    # plt.figure(figsize=(10, 3.5))
    #
    # plt.subplot(1, 2, 1)
    # plt.imshow(LinearGrid.T,extent=(utme_min,utme_max,utmn_min,utmn_max), origin='lower',cmap='jet')
    # plt.colorbar(extend='both')
    #
    # plt.subplot(1, 2, 2)
    # plt.imshow(CubicGrid.T,extent=(utme_min,utme_max,utmn_min,utmn_max), origin='lower', cmap='jet', norm =Normalize(-5,12))
    # plt.colorbar(extend='both')
    #
    # plt.show()