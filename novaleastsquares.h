#ifndef NOVALEASTSQUARES_H
#define NOVALEASTSQUARES_H
///----------------------------------------------------------------------------
/// Perform Least Squares with user's choice of float or double vectors.
/// Uses Eigen.
///----------------------------------------------------------------------------

#include <vector>
#include <optional>

#include <Eigen/Core>
#include <Eigen/Dense>

template <typename T>
class NovaLeastSquares
{
public:
    NovaLeastSquares(){};
    bool computeUsingNormalEq();
    std::optional<std::vector<T> > residualsAsPercentage();
    std::optional<std::vector<T> > residuals();
    std::optional<std::vector<T> > fit();
private:
    bool load(std::vector<std::vector<T> > &X, std::vector<T> &Y);
    bool load(std::vector<std::vector<T> > &X, std::vector<T> &Y,std::vector<T> &W);
    void switchToEigen(std::vector<std::vector<T> > &X, std::vector<T> &Y,std::vector<T> &W);
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Xd,Yd,Wd;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Bd; //soln
    bool doneCalculating;
};

#endif // NOVALEASTSQUARES_H
