#ifndef VECTORCOMPSTACKEDWAVEFORM_H
#define VECTORCOMPSTACKEDWAVEFORM_H

#include <vector>

#include "rstation.h"

class vectorCompStackedWaveform: public RStation
{
    ///"Vector Component Stacked Waveform" Data collected from a ground survey station from an EM coil in a
    /// specific orientation.
private:

    //permanent data
    std::vector<double> m_data; //Raw stacked waveform from survey ground station
    SVec m_orientation;        //Orientation of coil used to perform measurement. X-East,Y-North,Z-up
    vectorCompStackedWaveform();

public:
    vectorCompStackedWaveform(std::string stationName, SVec &location);//, std::vector<double> &data, SVec &orientation);
    void initialize(std::vector<double> &data, SVec &orientation);
    //setters and getters

    std::vector<double>::const_iterator begin();
    std::vector<double>::const_iterator end();
    const SVec &orientation() const;
    const std::vector<double> &data() const;
    std::vector<double> mCleanData; //Station waveform after cleaning
    void setOrientation(const SVec &newOrientation);
    void setData(const std::vector<double> &newData);
};

#endif // VECTORCOMPSTACKEDWAVEFORM_H
