#include "vectorcompstackedwaveform.h"
using namespace std;

void vectorCompStackedWaveform::setOrientation(const SVec &newOrientation)
{
    m_orientation = newOrientation;
}

void vectorCompStackedWaveform::setData(const std::vector<double> &newData)
{
    m_data = newData;
}

vectorCompStackedWaveform::vectorCompStackedWaveform(std::string stationName, SVec &location)://, std::vector<double> &data, SVec &orientation):
    RStation(location, stationName)
{
    mCleanData.assign(m_data.size(),0.0);
}

void vectorCompStackedWaveform::initialize(std::vector<double> &data, SVec &orientation)
{
    m_data=data;
    m_orientation=orientation;
}

vector<double>::const_iterator vectorCompStackedWaveform::begin()
{
    return m_data.cbegin();
}

vector<double>::const_iterator vectorCompStackedWaveform::end()
{
    return m_data.cend();
}

const std::vector<double> &vectorCompStackedWaveform::data() const
{
    return m_data;
}

const SVec &vectorCompStackedWaveform::orientation() const
{
    return m_orientation;
}
