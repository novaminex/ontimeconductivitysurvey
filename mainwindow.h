#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QWidget>
#include <QDir>
#include <vector>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
class QComboBox;
QT_END_NAMESPACE

class SVec;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_Go_clicked();
    void on_pushButton_Reorganize_clicked();
    void getVariablesFromIniFile();

private:
    Ui::MainWindow *ui;
    QComboBox *createComboBox(const QString &text = QString());

    QComboBox *fileComboBox;

    void findStackedFolder(QString dir);
    void findDatFile(QString dir);

    //    std::vector<SVec> openLoopFile(QString loopFileName);
};
#endif // MAINWINDOW_H
