#ifndef SM24_STK_READER_H
#define SM24_STK_READER_H
#include <filesystem>
#include "pugixml.hpp"

class SM24_stk_reader
{
    ///Open binary "stk" file produced by SM24 and extract "DataInfo.xml", "StackDataInfo.xml" and
    /// stacked waveform as vector<double>.
private:
    std::vector<size_t> CRFI; //"Carriage Return File Index" - The char index into stk file for the CR/LF values separating the xmls and the stacked waveform data.
    std::vector<double> stackedSeries;
    std::vector<char> dataInfo, stackDataInfo;
    std::filesystem::path datafilePath;
    bool initialized=false;
    pugi::xml_document loadXMLfileInPugi(std::filesystem::path sdi);
    std::string SDI_FileName();
    std::string DI_FileName();

public:
    SM24_stk_reader(){};        //default constructor
    void initialize(std::filesystem::path filepath);
    bool writeDataInfoXMLToFile();
    bool writeStackDataInfoXMLToFile();
    //    void changeOutputDir(std::filesystem::path outfilepath);
    const std::vector<double> &getStackedSeries() const;
    std::string getName();
    std::string getTime();
    std::string getLocation();
    void deleteDI();
};

#endif // SM24_STK_READER_H
